package dvs

author	Alina.Radu-Corso@cern.ch,Andrei.Kazarov@cern.ch
manager	Andrei.Kazarov@cern.ch

use ers
use system
#use tmgr
use ProcessManager
use is
#use rm
use DFConfiguration
use TestManager
		

pattern		dvs_kb_path	set		TDAQ_DVS_KB_PATH	<path>/installed/share/data
apply_pattern	dvs_kb_path

set		TDAQ_RLOGIN_OPTS	"-nf"
				  			  
#==================================
	private
#==================================

########################################################################################################
#	Libraries and apps
########################################################################################################
# needed on Ubuntu
macro_append    cppflags        ""      x86_64-slc6     " -B/usr/lib/x86_64-linux-gnu "

library		dvs	$(lib_opts)	../src/dvs*.cc ../src/manager.cc ../src/policies.cpp

macro_append    cppflags " -flto "

macro_append    pp_cppflags	" "			\
		debug		" -D__DEBUG__ "

macro		TEST_LIBS	"-lipc -lowl -lomniORB4 -lomnithread -lcmdline $(socket_libs) -lers"
macro		DVS_LIBS	"-ltbb -lnewtm -lnewtestdal -ldaq-df-dal -ldaq-core-dal \
				 -lconfig  $(TEST_LIBS) -lclips" 

macro		dvs_shlibflags	"$(DVS_LIBS)"

macro		lib_dvs_pp_cppflags	'-DDVS_DEFAULT_KB_PATH=\"$(datadir)\"	\
					 -DDVS_DEFAULT_INST_PATH=\"$(prefix)/\"'
# applications
application	dvs_console	-no_prototypes		../bin/dvs_console.cc		
application	dvs_print_def_host	-no_prototypes	../bin/dvs_print_def_host.cc		
application	dvs_get_hosts	-no_prototypes	../bin/dvs_get_hosts.cc		

macro		dvs_consolelinkopts 		"-ldvs -lprocessManagerClient $(DVS_LIBS) -lboost_system-$(boost_libsuffix) "
macro		dvs_console_dependencies 	dvs
macro		dvs_print_def_hostlinkopts 	"-ldaq-core-dal -lconfig \
						-lomnithread -lcmdline $(socket_libs) -losw -lers -lboost_system-$(boost_libsuffix) "
macro		dvs_get_hostslinkopts 		"-ldaq-core-dal -lconfig \
						-lomnithread -lcmdline $(socket_libs) -losw -lers -lboost_system-$(boost_libsuffix) "
########################################################################################################
#	Tests
########################################################################################################
# moved to dvs_tests package

########################################################################################################
#	 Java stuff
########################################################################################################
#replaced with QT4 implementation in dvs_gui package 


########################################################################################################
#	Installation patterns
########################################################################################################
#ignore_pattern  inst_headers_bin_auto
#ignore_pattern  inst_headers_auto

#apply_pattern   install_headers         src_dir="../dvs" files="local_tests.h"

apply_pattern   install_libs    	files="libdvs.so"

apply_pattern   install_apps		files="dvs_console dvs_print_def_host dvs_get_hosts"

apply_pattern	install_scripts		name=utils	src_dir=../bin files="dvs_init"

apply_pattern   install_data	        name=kb src_dir=../kb   target_dir=kb files="*.clp *.kb"

#apply_pattern   install_db_files        name=tr target_dir=sw files="../data/test-repository*.data.xml"

apply_pattern   install_examples     	name=database_examples	src_dir=../data   	\
					files="Tests-for-sunatdaq02.data.xml 		\
						Tests4computers.data.xml		\
						onlsw_test.data.xml			\
						detA_hardware.data.xml			\
						detA_segments.data.xml			\
						README" 				\
					target_dir=databases	

apply_pattern	check_target	name=dvs	file=../bin/check_dvs.sh
