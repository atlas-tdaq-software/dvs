#include <sstream>
#include <chrono>

#include <tmgr/tmresult.h>
#include <TM/Exceptions.h>
#include <TM/Client.h>

#include <dvs/dvscomponent.h>
#include <dvs/dvsmanager.h>

using namespace std::placeholders;
using namespace daq::dvs ;
using namespace daq::tmgr ;

// stuff related to starting test and handling the results

namespace daq::dvs {

// helper function to escape " and back-slash
void escape_json(std::string& str)
{
for ( auto esc: {'\\', '\"'} )
	{
	size_t p = 0 ;
	while ( (p = str.find(esc,p)) != std::string::npos )
		{ str.insert(p,1,'\\') ; p+=2 ; }
	}
}

// finds "token":"value" pairs, expects no spaces
std::string parse_json_simple(const std::string str, const std::string token)
{
auto p = str.find("\"" + token + "\"") ;
if ( p == std::string::npos )
	throw BadAction(ERS_HERE, "", "Failed to find token " + token + " in action parameters " + str) ;
auto term = str.find_first_of(",}", p + token.length() + 2) ;
auto start = str.find_first_of(":", p + token.length() + 2) ;
if ( start > term || start == std::string::npos || term == std::string::npos )
	throw BadAction(ERS_HERE, "", "Failed to parse token " + token + " in action parameters " + str) ;
auto start_quot = str.find_first_of("\"", start + 1) ;
auto end_quot = str.find_first_of("\"", start_quot + 1) ;
if ( start_quot == std::string::npos || term == std::string::npos )
	throw BadAction(ERS_HERE, "", "Failed to parse token " + token + " in action parameters " + str) ;
return str.substr(start_quot+1, end_quot-start_quot-1) ;
}

/**
 to be called from Engine, async no-return no-exept: all is handled via result
 */
void Component::startTest()
{
if ( testable_object_ == nullptr )
	{
	ERS_DEBUG(2, "Do nothing in startTest as there is no tests defined") ;
	is_testable_ = false ;
	return ;
	}

#ifdef __DEBUG__
auto start = std::chrono::high_resolution_clock::now() ;
#endif

ERS_DEBUG(1, "Launching testing of " << key() << " at scope " << getTestScope() << " with level " << getTestLevel()) ;

testComponent(m_test_scope, m_test_level, m_test_by_id) ;

#ifdef __DEBUG__
auto end = std::chrono::high_resolution_clock::now() ;
auto time = std::chrono::duration_cast<std::chrono::milliseconds>(end-start).count() ;
ERS_DEBUG(4, "CLIPS start test cb: Testing launched in " << time << " ms");
#endif
}


// TM callback std::function<void(const Result&)>
// testing of individual component is completed
// call setStatus (so the Engine judge the status of other components, start other tests etc),
// it will call Reactor::status()
// which will finally call user callbacks and fulfill the promise

void Component::testCompleted( const daq::tm::ComponentResult& result )
{
ERS_DEBUG(1, "Test for " << key() << " completed: " << result.result << " including " << result.test_results.size() << " results") ;

{ std::lock_guard<std::mutex> ml(m_update_mutex);
m_result.componentResult = result ; } // component result

setTestResult(m_result.componentResult.result) ; // single code result, signal the Engine


#ifdef __DEBUG__
auto end = std::chrono::system_clock::now() ;
auto time = std::chrono::duration_cast<std::chrono::milliseconds>(end-test_start_time_).count() ;
ERS_DEBUG(4, "Tests for " << key_ << " finished, took " << time << " ms");
#endif

switch ( result.result )
	{
	case TestResult::TmFail:
// check if there are follow-up Test actions 
		for ( const auto& tresult: result.test_results )
			{
ERS_DEBUG(2, "Test of " << key_ << " failed, got " << tresult.actions.size() << " upon failure actions") ;
			for ( const auto tfaction: tresult.actions ) // TestFailureAction
				{
				FailureAction action ;
				try {
				action.command = tfaction->get_action() ;
				action.description = tfaction->get_description() ;
				action.timeout = tfaction->get_timeout() ;
				action.parameters_json = tfaction->get_parameters() ; // e.g. { "Component": "#this.get_host().UID", "scope": "diagnosis" }
				DVSManager::getTestManager()->substituteParam(*testable_object_, action.parameters_json, true) ;
				
ERS_DEBUG(2, "Action parameters after substitution: " << action.parameters_json) ;
				if ( action.command == "execute" ) // needs special treatment for RC: extract all needed info from Executables
					{
					if ( tfaction->get_Runs().size() == 0 )
						{ 
						throw BadAction(ERS_HERE, tfaction->UID(), "0 Executables in Runs relationship")  ;
						}
					else if ( tfaction->get_Runs().size() > 1 )
						{
						throw BadAction(ERS_HERE, tfaction->UID(), "more then one Executable in Runs relationship")  ;
						}
					else 
						{
						auto exec = tfaction->get_Runs()[0] ;
						std::string program_uid = exec->get_Executes()->UID() ;
						std::string hostname = exec->get_Host() ;
						std::string exec_params = exec->get_Parameters() ;
						DVSManager::getTestManager()->substituteParam(*testable_object_, exec_params, true) ; // throw
						DVSManager::getTestManager()->substituteParam(*testable_object_, hostname, true) ;
						escape_json(exec_params) ;

						// finally build JSON for this Executable
						action.parameters_json.append("{").append("\"host\":").append("\"").append(hostname).append("\",") ; // host
						action.parameters_json.append("\"program_uid\":").append("\"").append(program_uid).append("\",") ; // program_uid
						action.parameters_json.append("\"parameters\":").append("\"").append(exec_params).append("\",") ; // parameters
						action.parameters_json.append("\"initTimeout\":").append(std::to_string(exec->get_InitTimeout())) ; // initTimeout			
						}
					action.parameters_json.append("}") ; // end JSON
ERS_DEBUG(2, "Crafted JSON exec action: " << action.parameters_json) ;
					} // "Execute" action
				else
				if ( action.command == "test" ) // expects parameters = { "Component": "#thisUID", "scope": "diagnosis" }
					{
					auto component = parse_json_simple(action.parameters_json, "component") ;
					auto scope = parse_json_simple(action.parameters_json, "scope") ;
					auto level = std::stoi(parse_json_simple(action.parameters_json, "level")) ;
					
					auto cmp = DVSManager::findComponent(component) ;
					if ( cmp == nullptr )
						throw BadAction(ERS_HERE, tfaction->UID(), "Failed to find Component to test " + component )  ;
					cmp->setTestScope(scope) ;
					cmp->setTestLevel(level) ;
					addTestFollowUp(cmp) ;
ERS_DEBUG(2, "Test follow-up component: " << cmp->key() ) ;
					} // "Test" action
				} catch ( const ers::Issue& ex )// print warning, add to issues in test result
					{
					ers::warning(ex) ;
					continue ; // do not add this actions
					}
				m_result.actions.push_back(action) ;
				} // for: actions
			} // for: test results

		break ; // test Failed

	case TestResult::TmUntested:
		{ std::stringstream combined_out ;
		for ( auto tr: result.test_results )
			{
			combined_out << "#########################\n\tTest " << tr.test_id << ": return code: " << tr.return_code << "\n" ;
			combined_out << "\tStdOut: " << tr.stdout << "\n" ;
			combined_out << "\tStdErr: " << tr.stderr << "\n" ;
			for ( const auto& ex: tr.issues )
				{
				combined_out << ex.get()->message() << "\n" ;
				}
			}

		ERS_DEBUG(0, "Failed to execute tests for component " << key() << ":\n" << combined_out.str() ); }
		break ; // not launched

	default: ;
	}
}

// interface to TM
// called from Engine for SE components
// async test launching, TMHandle stored
void Component::testComponent(const std::string& scope, int level, const std::string& test)
	{
	try {
	if ( test.empty() )
		{
		is_testable_ = false ;
		if ( testable_object_ == nullptr )
			{
			ERS_DEBUG(0, "Called testComponent for component without TestableObject! " << key_) ;	
			}
		else 
			{
			auto alltests = DVSManager::getTestManager()->getTestsForComponent(*testable_object_, scope, level) ;
			if ( !alltests.empty() )
				{
				is_testable_ = true ;
				}

			test_start_time_ = std::chrono::system_clock::now() ;
			daq::tm::Client* client = DVSManager::getTestManager() ;
			m_result.actions.clear() ;
			test_handle_.reset() ;
			test_handle_ = client->testComponent( *testable_object_, "", std::bind(&Component::testCompleted, this, std::placeholders::_1), scope, level ) ;

			ERS_DEBUG(2, "Testing launched for " << key_) ;	
			}
		}
	else
		{
		auto alltests = DVSManager::getTestManager()->getTestsForComponent(*testable_object_, scope, level) ;
		if ( alltests.empty() )
			{
			throw daq::tm::InconsistentData(ERS_HERE, "No tests for component found in TM", key_) ;
			}

		for ( auto daltest: alltests )
 			{
			if ( daltest->UID() == test )
				{
				test_handle_ = DVSManager::getTestManager()->testComponent(*testable_object_, test, std::bind(&Component::testCompleted, this, std::placeholders::_1) ) ;
				return ;
				}
			}
		throw daq::tm::InconsistentData(ERS_HERE, "No tests for component found in TM", test) ;
		}
	}
	catch ( const ers::Issue& ex ) // any kind of exception e.g. from config, dal etc used by TM 
		{
		std::ostringstream ostr;
		ostr << "Test was not started by Test Manager: <B><FONT COLOR=\"red\">";
		ostr << ex << "</FONT></B>).\n";
		std::string  msg = ostr.str();

		ers::warning(ers::Message(ERS_HERE, "Test was not started by Test Manager: ", ex));

		setTestResult(daq::tmgr::TestResult::TmUntested) ;

		// test was not started, no callback is expected, NOTDONE is sent to Engine
		ERS_DEBUG(2, "Test was not launched, sent TmUntested to " << key());
		}
	}

// uses stored TMHandle to interrupt the test
void Component::stopTest()
	{
	// if ( !test_handle_ ) return ; // nontestable
	auto handle = test_handle_.lock() ;
	if ( !handle ) return ;

	try {
		handle->stop() ; // shared_ptr, need to sync with start test?
		}
	catch ( ers::Issue& ex ) 
		{
		std::ostringstream msg ;
		msg << "Error in aborting the test for '" <<  key() << "': " << ex << std::endl;
		ers::warning(ers::Message(ERS_HERE, msg.str()));
		return ;
		}
		
	ERS_DEBUG(1, "Stopped test of " << key());
	}
} // namespace
