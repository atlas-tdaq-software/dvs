#include <unistd.h>

#include <list>
#include <set>

#include <iostream>

#include <ers/ers.h>
#include <config/Configuration.h>
#include <dal/Computer.h>
#include <dal/Module.h>
#include <dal/Crate.h>
#include <dal/Detector.h>
#include <dal/Application.h>
#include <dal/InfrastructureApplication.h>
#include <dal/ComputerProgram.h>
#include <dal/Resource.h>
#include <dal/Segment.h>
#include <dal/util.h>
#include <dal/InfrastructureTemplateApplication.h>

#include <TestManager/TestSet.h>
#include <system/Host.h>

#include <TM/Client.h>
#include <dvs/dvsenvironment.h>
#include <dvs/dvsmanager.h>
#include <dvs/Engine.h>

// DEBUG is controlled via req. file
#ifdef	__DEBUG__
#define	PMG_VRB		PMGVerbosity_VERBOSE
#else
#define	PMG_VRB		PMGVerbosity_SILENT
#endif

using std::string;
using namespace daq::dvs;

using ComponentsTests = std::map<std::string, TestsSet> ;	// map of ComponentID/TestSet

// static members definitions
const daq::core::Computer* DVSManager::default_host_;
bool DVSManager::verbosity_ = false; // level of verbosity for TM
Configuration* DVSManager::database_ = nullptr;
const daq::core::Partition* DVSManager::partition_ = nullptr;
daq::tm::Client * DVSManager::tmClient_ = nullptr ;

ComponentsMap DVSManager::all_components_ ;

std::map<std::string, const daq::tm::TestPolicy*> 	DVSManager::s_test_policies_class ;
std::map<std::string, const daq::tm::TestPolicy*> 	DVSManager::s_test_policies_object ;

/******************************************************************************************************************/
/**
 returns local host as daq::core::Computer*, or nullptr if it is not found in the configuration
 */
const daq::core::Computer* DVSManager::getLocalHost() const {

	const daq::core::Computer* local_host = 0;
	try {
		local_host = database_->get<daq::core::Computer> (System::LocalHost::full_local_name());
	} catch (daq::config::Exception &ex) {
		ers::warning(ers::Message(ERS_HERE, "Failed to contact database: ", ex));
		return nullptr;
	}

	return local_host;
}

/******************************************************************************************************************/
DVSManager::DVSManager() {
	// init heap-allocated objects
	status_ = Bad;
	partition_name_ = "";
	segment_name_ = "";
	use_config_ = true;
	load_all_ = true;
	verbosity_ = false;
	m_this_level_only = false;
	status_ = Ready;
	setBusy(false);
	substVar = 0;
//	dropHwOFF_ = true ;
	recursive_ = true ;
}


/******************************************************************************************************************/
/* deletes all database-dependent objects */
void DVSManager::dbCleanup() {
	Engine::instance().stop() ;
	ERS_DEBUG(1,"Engine suspended");

	// for ( auto c: all_components_ ) { if (c.first.find("CONFIGURATION_") == std::string::npos ) delete c.second ; } ;
	dvs_configuration_.reset() ; // release smart pointer, destruct components tree
	all_components_.clear() ; // map of component
	ERS_DEBUG(1, "Components tree cleared");

// all TestHandlers destroyed, now safe to delete TM client
	if ( tmClient_ ) {
		delete tmClient_;
		tmClient_ = nullptr ;
	}
// at this point no callbacks, no test exec threads
	ERS_DEBUG(1, "TM Client removed") ;

}

/******************************************************************************************************************/
DVSManager::~DVSManager() {	
	ERS_DEBUG(1,"DVSManager destructor clean-up (TM, removing Configuration, database)");
	Engine::instance().shutdown() ;
	dbCleanup();
	
	if (database_ && !use_config_) {
		delete database_;
		database_ = nullptr ;
		ERS_DEBUG(1, "Configuration removed");
	}
}

// try to set status to Busy or Ready
// return false if Manager already Busy
bool DVSManager::setBusy(bool flag) {
	std::lock_guard<std::mutex> ml(busy_mutex_);
	if (flag) {
		if (status_ == Busy)
			return false;
		else {
			status_ = Busy;
			return true;
		}
	}

	status_ = Ready;
	return true;
}

// Wrapper for Component
TestsSet DVSManager::getTestsForComponent(const Component* com, std::string scope, int level)
	{
	TestsSet ts ;
	if ( com->testableObject() != nullptr )
		{
		const daq::core::TestableObject* object = com->testableObject() ;
		ERS_DEBUG(5, "Getting tests for " << object->class_name() << "@" << object->UID()) ;
		if ( object != nullptr )
			{ TestsSet tests = tmClient_->getTestsForComponent(*object, scope, level) ;
			if ( !tests.empty() )
				{
				return tests ;
				}
			}
		}	
	return ts ;
	} 

/******************************************************************************************************************/
// synchronous reset
bool DVSManager::resetComponent(const std::string& id) {
	// set Busy and if it is already so, return Notdone
	if (!setBusy(true))
		{ return false ; }

	auto cmp = findComponent(id) ;
	if ( cmp != nullptr ) { cmp->resetComponent() ; } 

	setBusy(false);
	return true;
}

Component* DVSManager::findComponent(const std::string& id) 
{
// ComponentsMap::const_accessor result ;

auto it = all_components_.find(id) ;
if ( it != all_components_.end() ) 
	{ return it->second ; }
else
	{ return nullptr ; }

}

void DVSManager::addChild(Component* parent, Component* newchild, const daq::tm::TestBehavior* tb)
{
ERS_DEBUG(4, "Looking for existing component " << newchild->key());
auto it = all_components_.find(newchild->key()) ;
if ( it != all_components_.end() ) // already somewhere
	{
ERS_DEBUG(4, "Child " << it->second->key() << " already existed in the tree, adding ref to it and removing the fresh child");
		parent->addDeps(it->second) ;
		delete newchild ;
	}
else
	{
ERS_DEBUG(4, "Child " << newchild->key() << " added to " << parent->key() );
	all_components_.emplace(newchild->key(), newchild) ;
	parent->addChild(newchild, tb) ;
	}
}


/******************************************************************************************************************/
/* called by config when database is changed according to my criteria, see
 registerDatabaseCallback()  */
void DVSManager::reloadDatabaseCallback(const std::vector<ConfigurationChange *> &, void * mgr) {
	//	FIXME: need to handle reload callback if inference is going

	ERS_DEBUG(0, "Got DB changes notification callback -> reloading database");
	DVSManager* dvsmgr = reinterpret_cast<DVSManager*> (mgr);
	if (dvsmgr) {
		if (!dvsmgr->setBusy(true)) {
			ERS_DEBUG(0,"Got database change notification callback in Busy state, doing nothing.");
			return;
		}

		try {
			dvsmgr->reloadDatabase();
		} 
		catch (CannotReloadDatabase & ex) {
			dvsmgr->setBusy(false);
			ers::warning(ers::Message(ERS_HERE,"Failed to reload DVS in DB change notification callback: ",ex));
			return;
		}
		dvsmgr->setBusy(false);
		ERS_DEBUG(0, "Database reloaded");
	} else
		ers::warning(ers::Message(ERS_HERE,"Failed to get instance of DVSManager, database not reloaded. Please report a bug."));

}

/******************************************************************************************************************/
bool DVSManager::registerDatabaseCallback() {
	ERS_DEBUG(0, "Registering database callback");
	if ( database_ == nullptr )
		return false;

	::ConfigurationSubscriptionCriteria c1;

	if (database_->subscribe(c1, reloadDatabaseCallback,reinterpret_cast<void *> (this)) == 0) {
		ers::warning(ers::Message(ERS_HERE,"Failed to subscribe on database changes"));
		return false;
	}

	ERS_DEBUG(0, "Subscription OK");
	return true;
}

// TODO: in case a single segment was loaded, preserve this state upon reloading
/******************************************************************************************************************/
void DVSManager::reloadDatabase(bool skip_apps) // throw (CannotReloadDatabase )
{
	dbCleanup(); //delete tmClient_ and database object here

	if (!database_->loaded()) {
		throw CannotReloadDatabase(ERS_HERE, "Configuration is not reloaded.");
	}

	try {
		partition_ = daq::core::get_partition(*database_, partition_name_);
	}
	catch (daq::config::Exception & ex) {
		throw CannotReloadDatabase(ERS_HERE,"Can not retrieve partition from the database", ex);
	}
	substVar->reset(*partition_);

	s_test_policies_class.clear() ;
	s_test_policies_object.clear() ;

	skip_apps_ = skip_apps ;

	try {
		loadDatabase(database_, partition_name_, segment_name_, recursive_, skip_apps_);
	}
	catch (CannotLoadDatabase & ex) {
		status_ = Bad; // client can check the status after check_notification() call
		throw CannotReloadDatabase(ERS_HERE, ex);
	}

	Engine::instance().resume() ; // allow new event processing (the same rules loaded, old objects unregistred, new registered)
}

/******************************************************************************************************************/
void DVSManager::loadDatabase(Configuration* configuration, const std::string& partition, const std::string& my_segment, bool recursive, bool skip_apps)
{
	ERS_DEBUG(1, "loadDatabase entered");
	if ( configuration == 0 )
		{ throw CannotLoadDatabase(ERS_HERE, "Configuration is NULL.") ; } ;

	partition_name_ = partition ;
	segment_name_ = my_segment ;
 	recursive_ =  recursive ;
 	skip_apps_ =  skip_apps ;
	database_ = configuration;
	if (!database_->loaded()) {
		throw CannotLoadDatabase(ERS_HERE, "Configuration is not loaded.");
	}
	try {
		ERS_DEBUG(1, "get_partition called");
		partition_ = daq::core::get_partition(*database_, partition_name_);
	}
	catch (daq::config::Exception & ex) {
		throw CannotLoadDatabase(ERS_HERE,"Can not retrieve partitions from the database", ex);
	}

	if (!partition_) {
		std::ostringstream text;
		text << "Partition " << partition_name_	<< " not found in the configuration";
		throw CannotLoadDatabase(ERS_HERE, text.str());
	}

	ERS_DEBUG(1, "Partition '" << partition_name_ << "' found");

	if (!substVar) {
		substVar = new daq::core::SubstituteVariables(*partition_);
		// register variables converter
		try {
			database_->register_converter(substVar);
		}
		catch (daq::config::Exception & ex) {
			throw CannotLoadDatabase(ERS_HERE,"Can not substitute database variables", ex);
		}
	}

	std::mutex db_mutex; // lock access to Configuration if it is reloaded externally
	std::lock_guard<std::mutex> dbmutex(db_mutex);

	ERS_DEBUG(1, "Loading database from Configuration: " << partition_name_);

	// Init Test Manager with just loaded database
	try {
		tmClient_ = new daq::tm::Client(*database_, *partition_) ;
	}
	catch (const ers::Exception & ex) {
		throw CannotLoadDatabase(ERS_HERE, "Failed to initialize TestManager", ex);
	}

	ERS_DEBUG(1, "TMClient initialized");

	try {
		if (!(online_segment_ = partition_->get_OnlineInfrastructure())) {
			std::ostringstream text;
			text << "OnlineInfrastructure is not defined for partition "
					<< partition_->UID() << ". Check your configuration.";
			throw CannotLoadDatabase(ERS_HERE, text.str());
		}

		if ( partition_name_ == "initial" )
			initial_partition_ = partition_;
		else
			initial_partition_ = online_segment_->get_InitialPartition();

		if (!initial_partition_)
			throw CannotLoadDatabase(ERS_HERE,"InitialPartition is not defined for OnlineInfrastructure segment");

		if (!(initial_infrastructutre_	= initial_partition_->get_OnlineInfrastructure()))
			throw CannotLoadDatabase(ERS_HERE,"OnlineInfrastructure relationship is empty in initial partition");
	}
	catch (daq::config::Exception & ex) {
		throw CannotLoadDatabase(ERS_HERE, "Can not retrieve information from the database", ex);
	}

	tmClient_->setDiscardLogs(false) ;
	tmClient_->setVerbose(false) ;

	std::set<const daq::core::TestRepository*> testreps ;
	testreps.insert(online_segment_->get_TestRepositories().begin(), online_segment_->get_TestRepositories().end())  ;
	testreps.insert(partition_->get_TestRepositories().begin(), partition_->get_TestRepositories().end()) ;

	for ( auto testrep: testreps )
		{
		const daq::tm::TestSet* test_set = database_->cast<daq::tm::TestSet, daq::core::TestRepository> (testrep);
		if ( test_set )
			{
			for ( auto test_pol: test_set->get_TestPolicies() )
				{
				const daq::tm::TestPolicy4Class * tpclass = database_->cast<daq::tm::TestPolicy4Class, daq::tm::TestPolicy>(test_pol) ;
				if ( tpclass )
					{
					auto it = s_test_policies_class.find(tpclass->get_Class()) ;
					if ( it != s_test_policies_class.end() )
						{
						throw CannotLoadDatabase(ERS_HERE, "Failed to configure Test Policy for class " + tpclass->get_Class(),
						BadTestPolicy(ERS_HERE, test_pol->UID(), "Test Policy for this class already defined.")) ;
						}
					s_test_policies_class[tpclass->get_Class()] = tpclass ;
					}
				else 	{
					const daq::tm::TestPolicy4Object * tpobject = database_->cast<daq::tm::TestPolicy4Object, daq::tm::TestPolicy>(test_pol) ;
					if ( tpobject )
						{
						for ( auto to: tpobject->get_Objects() )
							{
							s_test_policies_object[to->UID()] = tpobject ; // assume unique OKS TestableObjects
							}
						}
					}
				}
			}
		else
			{ throw CannotLoadDatabase(ERS_HERE, "Can not cast TestRepository to TestSet") ; }
		}

	ERS_DEBUG(1, "TestPolicies loaded: " << s_test_policies_class.size() << " policies for class found");
	ERS_DEBUG(1, "TestPolicies loaded: " << s_test_policies_object.size() << " policies for object found");

	// Check that we have at least a valid host!
	default_host_ = partition_->get_DefaultHost() ? partition_->get_DefaultHost(): getLocalHost();
	ERS_DEBUG(1, "Got Default Host from database");

	if (!default_host_) {
		throw CannotLoadDatabase(ERS_HERE, "Neither partition Default host nor local host are defined in the database.");
	}

	dvs_configuration_ = std::shared_ptr<Component>(new Component(partition_->UID(), Component::Configuration));

	try {
		// always load Online Infrastructure
	const daq::core::Segment *some_seg, *my_seg ;
	ERS_DEBUG(1, "Getting Online Segment from database");
	some_seg = partition_->get_segment(online_segment_->UID()) ; 
	const daq::core::OnlineSegment* onl_seg = some_seg->cast<daq::core::OnlineSegment>() ;

	if ( my_segment.empty() ) // load whole partition, recursively
		{
		loadSegment(onl_seg, dvs_configuration_.get(), false) ; 
	ERS_DEBUG(1, "OnlineSegment loaded");

		// load all other Segments
	ERS_DEBUG(1, "Loading whole partition");
		for ( auto a_seg: onl_seg->get_nested_segments() )
			{ loadSegment(a_seg, dvs_configuration_.get(), true); }
		}
	else  // load from a Segment level, either recursively (dvs) or not (rc)
		{
		ERS_DEBUG(1, "Getting Segment " << my_segment << " from database");
		my_seg = partition_->get_segment(my_segment) ; 
	//	if ( my_seg.get_segment().class_name() != "OnlineSegment" )
			{
			ERS_DEBUG(1, "Loading Segment " << my_segment << " recursively: " << recursive);
			loadSegment(my_seg, dvs_configuration_.get(), recursive) ;
			}
		}
	} // load configuration
	catch (ers::Issue & ex) {
		throw CannotLoadDatabase(ERS_HERE, "Failed to load components tree", ex);
	}

	ERS_DEBUG(1, "Configuration loaded");

	all_components_.emplace(dvs_configuration_->key(), dvs_configuration_.get()) ;
	// all_components_.rehash() ;
}

