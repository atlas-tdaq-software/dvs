/*
changes:
AK 29082002: _kb_path not derived from TDAQ_INST_PATH, TDAQ_DVS_KB_PATH shall be always defined
*/

#include <stdlib.h>
#include <iostream>

#include <ers/ers.h>
#include <dvs/dvsenvironment.h>

using namespace daq::dvs ;

std::string		DVSEnvironment::kb_path_ ;
std::string		DVSEnvironment::exec_root_  ;
std::string		DVSEnvironment::help_root_ ;
int			DVSEnvironment::max_tests_ ;
std::string		DVSEnvironment::logs_path_ ;
std::string		DVSEnvironment::ipc_ref_file_ ;
bool			DVSEnvironment::initialized_ = false ;	

void DVSEnvironment::initialize()
{
	if ( initialized_ ) return ;

	// kb_path_	= DVS_DEFAULT_KB_PATH ;
	ipc_ref_file_	= "" ;
	max_tests_	= 30 ;
	logs_path_	= "/tmp" ;
	help_root_	= "http://atddoc.cern.ch/Atlas/Notes" ;
	
        const char * var;
        
        if ( (var = getenv("TDAQ_INST_PATH")) )
		exec_root_ = var;

        if ( (var = getenv("TDAQ_DVS_KB_PATH")) )
                { kb_path_ = var; }
	else
		{ kb_path_ = exec_root_ + "/shared/data" ; }

        if ( (var  = getenv("TDAQ_DVS_HELP")) )
                help_root_ = var;

	if ( (var = getenv("TDAQ_LOGS_PATH")) ) 
		logs_path_ = var ;

	if ( (var = getenv("TDAQ_DVS_MAX_TESTS")) ) 
		max_tests_ = atoi(var) ;

	if ( (var = getenv("TDAQ_IPC_INIT_REF")) ) 
		ipc_ref_file_ = var ;
	else
		ERS_DEBUG(0, "No TDAQ_IPC_INIT_REF defined in your environment! ") ;
		
        initialized_ = true;
}

const std::string& DVSEnvironment::kbRoot()
{
	if ( !initialized_ )
		initialize();
	return kb_path_;
}

const std::string& DVSEnvironment::execRoot()
{
	if ( !initialized_ )
		initialize();
	return exec_root_;
}

const std::string& DVSEnvironment::helpRoot()
{
	if ( !initialized_ )
		initialize();
	return help_root_;
}

int DVSEnvironment::maxTests()
{
	if ( !initialized_ )
		initialize();
	return max_tests_;
}

const std::string& DVSEnvironment::logsPath()
{
	if ( !initialized_ )
		initialize();
	return logs_path_;
}

const std::string& DVSEnvironment::ipcRefFile()
{
	if ( !initialized_ )
		initialize();
	return ipc_ref_file_;
}
