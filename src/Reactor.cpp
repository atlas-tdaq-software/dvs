#include <ers/ers.h>

#include <dvs/Reactor.h>
#include <dvs/Engine.h>

using namespace daq::dvs ;
using namespace std::chrono ;

// register instance in the engine
Reactor::Reactor()
	{
	m_deps.reserve(64) ;
	auto now = time_point_cast<microseconds>(system_clock::now());
	TimestampT noww = now.time_since_epoch().count() ;
	m_state_timestamp.store(noww) ;
	Engine::instance().register_object(this) ;
	} 

Reactor::~Reactor() 
	{
	Engine::instance().unregister_object(this) ;	
	}

// sends update event to the engine, making it to apply rules to this updated object
void Reactor::send_event()
	{
	auto now = time_point_cast<microseconds>(system_clock::now());
	TimestampT noww = now.time_since_epoch().count() ;
	set_timestamp(noww) ; 
	Engine::instance().add_event(noww) ;
	ERS_DEBUG(5, "Event sent" ) ;
	} 

// returns the most recent timestamp of the dependencies of this object
TimestampT
Reactor::get_state_timestamp() 
	{
	get_dependencies(m_deps) ; // derived class implementation

	if ( m_deps.empty() )
	 	{ return get_timestamp() ; }

	auto res = std::max_element(m_deps.begin(), m_deps.end(), [](auto& e1, auto& e2){ return e1->get_timestamp() < e2->get_timestamp();} );
	m_deps.clear() ;
	return std::max(get_timestamp(), (*res)->get_timestamp()) ;
	}
