/*

 DVSManager functions to load HW tree of *TESTABLE* HW_Systems, Resources and HW_Objects

 */
#include <ers/ers.h>
#include <dal/HW_System.h>
#include <dal/HW_Object.h>
#include <dal/Rack.h>
#include <dal/Crate.h>
#include <dal/Module.h>
#include <dal/Interface.h>
#include <dal/NetworkInterface.h>
#include <dal/Computer.h>
#include <dal/ComputerBase.h>
#include <dal/ComputerSet.h>
#include <dal/Application.h>
#include <dal/TemplateSegment.h>
#include <dal/InfrastructureApplication.h>
#include <TestManager/TestBehavior.h>
#include <TM/Client.h>

#include <DFdal/HW_InputChannel.h>

#include <dvs/dvscomponent.h>
#include <dvs/dvsmanager.h>

using namespace daq::dvs;

// parent: DalObject, group: it's child, Composite corresponding to a TP, objects: child DalObjects for this group
bool DVSManager::loadSubtree(Component* parent, Component* group, std::vector<const DalObject*> objects, bool recursive)
{
bool group_added = false ;

for ( const auto tobject: objects )
	{
ERS_DEBUG(4, "Loading dal Object "<< tobject->UID() << " to " << parent->key() << "@" << group->key()) ;
		auto [iter, result] = loaded_stack.insert(tobject->UID() + tobject->class_name()) ;
		if ( result == false )
			{
			throw BadTestPolicy(ERS_HERE, group->key(), "Looping TestPolicies when loading object " + tobject->UID()) ;
			}
		
		TestsSet tests_for_obj ;
	    if ( database_->try_cast("TestableObject", tobject->class_name()) ) {
			const daq::core::TestableObject* to = database_->get<daq::core::TestableObject> (tobject->UID());
		  	if (to) {
				tests_for_obj = tmClient_->getTestsForComponent(*to, "any", 255) ;
				ERS_DEBUG(3,"Testable Object: "<< tobject->UID() << " has " << tests_for_obj.size() << " tests defined") ;
			}
		} else {
				ERS_DEBUG(4, "Object "<< tobject->UID() << " is not TestableObject, no tests will be loaded for it") ;
		}

		if ( !recursive && database_->try_cast(daq::core::Segment::s_class_name, tobject->class_name()) )
			{
			ERS_DEBUG(2, "Skipped Segment "<< tobject->UID() << " in non-recursive mode") ;
			continue ;
			}
		
				// e.g. Segment->Infrastructure->Application1->RunsOn->Comp1: skip Application->RunsOn
				// to get Segment->Infrastructure->{Comp1, Comp2...}
	    if ( skip_apps_ && database_->try_cast(daq::core::BaseApplication::s_class_name, tobject->class_name()) )
			{
ERS_DEBUG(4, "Skipping loading of "<< tobject->UID() << ": this is s/w, adding its children directly to " << parent->key() << " policy: " << group->key()) ;
			for ( const auto& adeps: loadByTestPolicies(tobject) ) // list of pair Policy, list e.g. {{'RunsOn'; {'pc-tdq-onl-01',...}};...}
				{
				if ( loadSubtree(parent, group, adeps.second) )
					{ group_added = true ; }
				}
			loaded_stack.erase(iter) ;
			continue ; // next tobject 
			}
		
		if ( const daq::core::HW_Object* hwobj = tobject->cast<daq::core::HW_Object>() ) {
			if ( hwobj->get_State() == false ) {
				ERS_DEBUG(2, "Skipped HW_Object "<< tobject->UID() << " which is Off") ;
				loaded_stack.erase(iter) ;
				continue ;
			}
		}

		Component::Type type = Component::CompositeEntity;
		std::string ouid = tobject->UID() ;
		if ( database_->try_cast(daq::core::Crate::s_class_name, tobject->class_name()) )
			{
			}

		if ( database_->try_cast(daq::core::Module::s_class_name, tobject->class_name()) ) 
			{ 
			}

		if ( database_->try_cast(daq::core::Computer::s_class_name, tobject->class_name()) )
			{
			type = Component::Computer ;
			}

		if ( database_->try_cast(daq::core::BaseApplication::s_class_name, tobject->class_name()) )
			{
			type = Component::Application ;
			}

		if ( database_->try_cast(daq::df::HW_InputChannel::s_class_name, tobject->class_name()) )
			{
			}

		// protect from Detector ID = Segment ID = App ID, filter out ConfigObjects which can not have tests
		if ( database_->try_cast("Segment", 	tobject->class_name()) ||
		     database_->try_cast("HW_System",	tobject->class_name()) ) 
			{ ouid = tobject->class_name() + "@" +  tobject->UID() ; }
	
		bool object_added = false ;
		Component* c_component = new Component(ouid, type, tobject) ;
		object_added = loadDalObject(c_component, tobject) ;

		if ( object_added || tests_for_obj.size() )
			{
ERS_DEBUG(4, "Child "<< c_component->key() << " loaded") ;
			addChild(group, c_component) ;
			group_added = true ;
			}
		else
			{
ERS_DEBUG(4, "Child "<< c_component->key() << " NOT loaded") ;
			delete c_component ;
			}

		loaded_stack.erase(iter) ;
	} // for dal objects

if ( group_added )
	{
ERS_DEBUG(4, "Group "<< group->key() << " added") ;
	}

return group_added ;
}

bool DVSManager::loadDalObject(Component* c_component, const DalObject* dalobject, bool recursive)
{
	auto depslist = loadByTestPolicies(dalobject) ; // recursion, find children by TPolicies
ERS_DEBUG(4, "Loading " << depslist.size() << " dependencies groups of " << dalobject->UID()) ;

	bool added = false ;
	for ( const auto& adeps: depslist ) // adeps: pair<Policy, vector<dalobj>>
		{
		if ( !recursive && (adeps.first->get_RelationshipName() == "get_nested_segments()") ) {
			ERS_DEBUG(1, "Skipping loading of nested segments in non-recursive mode") ;
			continue ; 
		}
		ERS_DEBUG(5, "Loading " << adeps.second.size() << " dependencies of " << dalobject->UID() << " in group " << adeps.first->UID()) ;
		Component* c_group = new Component(c_component->key(), *adeps.first) ;
		if ( loadSubtree(c_component, c_group, adeps.second) )
			{
			ERS_DEBUG(4, "Group " << dalobject->UID() << "@" << adeps.first->UID() << " added") ;
			addChild(c_component, c_group) ;
			added = true ; 
			}
		else
			{
			ERS_DEBUG(4, "Group " << dalobject->UID() << "@" << adeps.first->UID() << " NOT added") ;
			delete c_group ;	
			} 
		}
	return added ;
}

/** 	Load all s/w and h/w in a Segment and in all child Segments recursively
 \return false if no applications of child segments is added to this segment
 */
bool DVSManager::loadSegment(const daq::core::Segment* segment, Component* c_parent, bool recursive) // throw (CannotLoadInstance )
{
	if ( segment->is_disabled() ) {
		ERS_DEBUG(1, "Segment " << segment->UID() << " is disabled, skipping it...");
		return false;
	}
	ERS_DEBUG(1, "Loading segment " << segment->UID() << " as parent of " << c_parent->key());

	// TODO: nicer constructor for Segment
	Component* c_segment = new Component("Segment_" + segment->UID(), Component::CompositeEntity, segment) ;
	if ( loadDalObject(c_segment, segment, recursive) ) {
		ERS_DEBUG(2, "Adding segment " << c_segment->key() << " to parent " << c_parent->key());
		addChild( c_parent, c_segment); 
		return true;
	} else {
		ERS_DEBUG(1, "Not adding segment, as there is nothing in it.");
		delete c_segment;
		return false;
	}
}
