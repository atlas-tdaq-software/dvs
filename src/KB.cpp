#include <dvs/Rule.h>
#include <dvs/dvscomponent.h>

using namespace daq::dvs ;
using namespace daq::tmgr ;//  { TmPass, TmFail ... }

using daq::dvs::Component ;

//#define DBGR(component, message) std::cerr << "RULE DBG: " << component->key() << ": " << message <<std::endl ;
#define DBGR(component, message)
// static stuff
// Component::rule_loader Component::loader ;

/*
Predicate2<ComponentRef> fire_always_p = 
[](ComponentRef a) { return std::make_tuple(
					[=] { return true ; },
					[=]	{ std::cout << "DBG: " << a->key() << " updated:" << a->getStatus() << std::endl ; }
					) ;
		    } ;
*/

//Rule<ComponentRef> fire_on_update ("someid", fire_always_p) ;
/*
DEFRULE ( print_passed, 0, ComponentRef, a, 
			LHS( a->getStatus() == Component::PASSED ; )
			RHS( std::cerr << a->key() << " Passed!\n" ; ) )

DEFRULE ( print_failed, 0, ComponentRef, a,
			LHS( a->getStatus() == Component::FAILED ; )
			RHS( std::cerr << a->key() << " Failed!\n" ; )
			) 
*/

// leaf component tested completed with some result, need to define global status
DEFRULE ( leaf_local_test_completed, 0, ComponentRef, a,
			LHS(
				a->getTestResult() != TmUndef
				&& (a->getCommand() == Component::Command::Test || a->getCommand() == Component::Command::TestById)
				&& a->dependencies().empty(); )
			RHS(
				DBGR(a, "Leaf test completed, can define global result" )
				a->setCommand( Component::Command::None ) ;
				a->setStatus((Component::Status)a->getTestResult()) ;
			) 
		)

// TestByID completed 
DEFRULE ( local_testbyid_completed, 0, ComponentRef, a,
			LHS(
				a->getTestResult() != TmUndef
				&& a->getCommand() == Component::Command::TestById ; )
			RHS(
				DBGR(a, "TestByID test completed, can define global result" )
				a->setCommand( Component::Command::None ) ;
				a->setStatus((Component::Status)a->getTestResult()) ;
			) 
		)

// propagate Test command to dependencies in ASYNC mode
DEFRULE ( test_command_async, 10, ComponentRef, a,
			LHS( a->getCommand() == Component::Command::Test
				 // && a->getStatus() == Component::UNDEFINED
				 && a->getStatus() != Component::INPROGRESS
				 && a->SyncPolicy() == false ; )
			RHS(
				a->setStatus(Component::INPROGRESS) ;
				DBGR(a, "Testing all dependencies, my status: " << a->getStatus())  
				std::for_each(a->dependencies().begin(), a->dependencies().end(), [](auto i)
								{
								if ( i->getCommand() == Component::Command::None ) // check if not already being tested
									{
									i->setCommand( Component::Command::Test ) ;
									i->setStatus(Component::UNDEFINED) ;
									i->setTestResult(TmUndef) ;
									DBGR(i, "Started testing child")
									}
								else
									{
									DBGR(i, "Child is already under testing")	
									}
								} ) ;
				DBGR(a, "Launching local test") 
				a->setTestResult(TmUndef) ;
				a->startTest() ; )
		) 



// start local test in Sync/First mode
DEFRULE ( test_sync_first_self, 0, ComponentRef, a,
			LHS( a->getCommand() == Component::Command::Test
				 && a->getStatus() == Component::UNDEFINED
				 && a->SyncPolicy() == true 
				 && a->ParentTestOrder() == Component::First ; )
			RHS(
				a->setStatus(Component::INPROGRESS) ; //?
				DBGR(a, "Launching local test in Sync/First mode") 
				a->startTest() ;
				std::for_each(a->dependencies().begin(), a->dependencies().end(), [](auto i)
					{
					if ( i->getStatus() != Component::INPROGRESS )
						{
						i->setStatus(Component::UNDEFINED) ; 
						i->setTestResult(TmUndef) ;
						}
					} ) ;
				)
		) 

// start next child test in Sync/First mode, NoStopOnError (local test done)
DEFRULE ( test_sync_first_nostop_next_child, 0, ComponentRef, a,
			LHS( a->getCommand() == Component::Command::Test
				 && (a->getTestResult() != TmUndef || !a->isTestable())  // local test completed
				 && a->getStatus() == Component::INPROGRESS
				 && a->SyncPolicy() == true 
				 && a->StopOnError() == false 
				 && a->ParentTestOrder() == Component::First 
				 && !a->dependencies().empty()
				 && std::none_of( a->dependencies().begin(), a->dependencies().end(),
 					[](auto i) { return i->getStatus() == Component::INPROGRESS
					 				 || i->getCommand() == Component::Command::Test ; })  ;)  // next child ready
			RHS(
				auto next = std::find_if(a->dependencies().begin(), a->dependencies().end(),
								[](auto i) { return i->getStatus() == Component::UNDEFINED ;} ) ;
				if ( next != a->dependencies().end() )
					{
					DBGR(a, "Testing in Sync/First mode continued: testing next child " << (*next)->key() )
					(*next)->setCommand(Component::Command::Test) ;
					}
				else
					{
					DBGR(a, "No next child to test in Sync/First, apparently another rule is to trigger..")
					} ; )
		) 

// self test Failed Sync/First mode
DEFRULE ( test_sync_first_nostop_self_failed, 0, ComponentRef, a,
			LHS( a->getCommand() == Component::Command::Test
				 && a->isTestable()
				 && a->getTestResult() != TmPass 
				 && a->getStatus() == Component::INPROGRESS
				 && a->SyncPolicy() == true 
				 && a->StopOnError() == true 
				 && a->ParentTestOrder() == Component::First ; )
			RHS(
				DBGR(a, "Testing in Sync/First mode completed StopOnError: local test non-PASSED " )
				a->setCommand(Component::Command::None) ;
				a->setStatus(Component::FAILED) ; )
		) 

// start child testing in Sync/Last mode
DEFRULE ( test_sync_last_start_child, 0, ComponentRef, a,
			LHS( a->getCommand() == Component::Command::Test
				 && a->getStatus() == Component::UNDEFINED
				 && a->SyncPolicy() == true 
				 && a->ParentTestOrder() == Component::Last ; )
			RHS(
				DBGR(a, "Starting children testing in Sync/Last mode") 
				a->setStatus(Component::INPROGRESS) ;
				std::for_each(a->dependencies().begin(), a->dependencies().end(), [](auto i)
					{
					if ( i->getStatus() != Component::INPROGRESS )
						{
						i->setStatus(Component::UNDEFINED) ; 
						i->setTestResult(TmUndef) ;
						}
					} ) ;

				auto first = a->dependencies().front() ;
				if ( first->getStatus() != Component::INPROGRESS )
					{ first->setCommand(Component::Command::Test) ; } )
		) 

// next child in Sync/NostopOnError mode
DEFRULE ( test_sync_next_child_nostoponerror, 0, ComponentRef, a,
			LHS( a->getCommand() == Component::Command::Test
				 // && a->getStatus() == Component::INPROGRESS
				 && a->SyncPolicy() == true 
				 && a->StopOnError() == false 
				 // && a->ParentTestOrder() == Component::Last 
				 && std::none_of( a->dependencies().begin(), a->dependencies().end(),
 					[](auto i) { return i->getStatus() == Component::INPROGRESS
					 					|| i->getCommand() == Component::Command::Test ; })
				 && std::any_of( a->dependencies().begin(), a->dependencies().end(),
 					[](auto i) { return i->getStatus() == Component::UNDEFINED ; })  ; )
			RHS(
				auto next = std::find_if(a->dependencies().begin(), a->dependencies().end(),
								[](auto i) { return i->getStatus() == Component::UNDEFINED ;} ) ;
				
				if ( next != a->dependencies().end() )
					{
					DBGR(a, "Testing in Sync mode continued: testing next child " << (*next)->key() )
					(*next)->setCommand(Component::Command::Test) ;
					}
				else
					{
					DBGR(a, "Shall not be here apparently another rule is to trigger..")
					} ; )
		) 

// next child in Sync/NostopOnError mode
DEFRULE ( test_sync_next_child_stoponerror, 0, ComponentRef, a,
			LHS( a->getCommand() == Component::Command::Test
				 // && a->getStatus() == Component::INPROGRESS
				 && a->SyncPolicy() == true 
				 && a->StopOnError() == true 
				 // && a->ParentTestOrder() == Component::Last 
				 && std::none_of( a->dependencies().begin(), a->dependencies().end(),
 					[](auto i) { return i->getStatus() == Component::INPROGRESS
					 					|| i->getCommand() == Component::Command::Test 
										|| i->getStatus() == Component::FAILED 
										 || i->getStatus() == Component::INPROGRESS ; })
				 && std::any_of( a->dependencies().begin(), a->dependencies().end(),
 					[](auto i) { return i->getStatus() == Component::UNDEFINED ; })  ; )
			RHS(
				auto next = std::find_if(a->dependencies().begin(), a->dependencies().end(),
								[](auto i) { return i->getStatus() == Component::UNDEFINED ;} ) ;
				
				if ( next != a->dependencies().end() )
					{
					DBGR(a, "Testing in Sync mode continued: testing next child " << (*next)->key() )
					(*next)->setCommand(Component::Command::Test) ;
					}
				else
					{
					DBGR(a, "Shall not be here apparently another rule is to trigger..")
					} ; )
		) 

// complete testing in StopOnError if a child non-PASSED
DEFRULE ( test_sync_completed_stoponerror, 0, ComponentRef, a,
			LHS( a->getCommand() == Component::Command::Test
				 // && a->getStatus() == Component::INPROGRESS
				 && a->SyncPolicy() == true 
				 && a->StopOnError() == true 
				 // && a->ParentTestOrder() == Component::Last 
				 && std::none_of( a->dependencies().begin(), a->dependencies().end(),
 					[](auto i) { return i->getStatus() == Component::INPROGRESS
					 					|| i->getCommand() == Component::Command::Test ; })
				 && std::any_of( a->dependencies().begin(), a->dependencies().end(),
 					[](auto i) { return i->getStatus() == Component::FAILED ||
					 					i->getStatus() == Component::UNRESOLVED ; })  ; )
			RHS(
				DBGR(a, "Completed testing in StopOnError Sync mode, a child FAILED")
				a->setCommand(Component::Command::None) ;
				a->setStatus(Component::FAILED) ; )
		) 


// all children tested in Sync/Last mode -> test parent
DEFRULE ( test_sync_last_parent, 0, ComponentRef, a,
			LHS( a->getCommand() == Component::Command::Test
				 && a->getStatus() == Component::INPROGRESS
				 && a->getTestResult() == TmUndef
				 && a->SyncPolicy() == true 
				 && a->ParentTestOrder() == Component::Last 
				 && std::none_of( a->dependencies().begin(), a->dependencies().end(),
 					[](auto i) { return i->getStatus() == Component::INPROGRESS 
					 					|| i->getStatus() == Component::UNDEFINED ; })  ; )
			RHS(
				DBGR(a, "Testing in Sync/Last mode: all children ready, test parent last")
				a->startTest() ; )
		) 

// single TestById
DEFRULE ( test_by_id, 0, ComponentRef, a,
			LHS( a->getCommand() == Component::Command::TestById
				 && a->getStatus() == Component::UNDEFINED ; )
			RHS(
				a->setStatus(Component::INPROGRESS) ;
				DBGR(a, "Launching local test by ID") 
				a->setTestResult(TmUndef) ;
				a->startTest() ;
				)
		) 

// propagate Stop command to dependencies 
DEFRULE ( stop_command, 0, ComponentRef, a,
			LHS( a->getCommand() == Component::Command::Stop
				 && a->getStatus() == Component::INPROGRESS ; )
			RHS(
				DBGR(a, "Stopping testing")  
				std::for_each(a->dependencies().begin(), a->dependencies().end(), [](auto i)
								{ i->setCommand( Component::Command::Stop ) ; } ) ;
				a->setCommand(Component::Command::Test) ;
				a->stopTest() ;
				)
		) 

// all dependencies Passed (or can be ignored), local test Passed (or not testable), non-Application
DEFRULE ( all_dependencies_passed, 7, ComponentRef, a,
			LHS ( !a->dependencies().empty() &&
				  std::all_of( a->dependencies().begin(), a->dependencies().end(),
					[a](auto i) 	{ return
									i->getStatus() == Component::PASSED ||
									i->getStatus() == Component::UNSUPPORTED ||
									( (i->getStatus() == Component::FAILED || i->getStatus() == Component::UNRESOLVED )
									  && a->isBackupHost(i->key()));
									} ) &&
				  a->getCommand() == Component::Command::Test &&
				  ( !a->isTestable() || (a->isTestable() && (a->getTestResult() == TmPass))) ;
			)
			RHS (
				DBGR(a, "testing completed, parent Passed!") ;
				a->setStatus(Component::PASSED) ;
				a->setCommand( Component::Command::None ) ;
				)
		) 


// testing complete, at least one child failed
DEFRULE ( one_child_failed, 5, ComponentRef, a,
			LHS ( !a->dependencies().empty() &&
				std::any_of( a->dependencies().begin(), a->dependencies().end(),
					[](auto i) { return i->getStatus() == Component::FAILED ; }) &&
				std::none_of( a->dependencies().begin(), a->dependencies().end(),
 					[](auto i) { return i->getStatus() == Component::INPROGRESS || i->getStatus() == Component::UNDEFINED; }) 
				&& a->getCommand() == Component::Command::Test 
				&& ( !a->isTestable() || (a->isTestable() &&  a->getTestResult() != TmUndef)) ;
			)
			RHS (
				DBGR(a, "Parent Failed (one of dependencies Failed)!") ;
				a->setStatus( Component::FAILED ) ;
				a->setCommand( Component::Command::None ) ;
				) ) 

// testing complete, one child unresolved
DEFRULE ( one_child_unresolved, 0, ComponentRef, a,
			LHS ( !a->dependencies().empty() &&
				std::any_of( a->dependencies().begin(), a->dependencies().end(),
					[](auto i) { return i->getStatus() == Component::UNRESOLVED || i->getStatus() == Component::UNTESTED ; }) &&
				std::none_of( a->dependencies().begin(), a->dependencies().end(),
 					[](auto i) { return i->getStatus() == Component::INPROGRESS || i->getStatus() == Component::UNDEFINED; }) 
				&& a->getCommand() == Component::Command::Test 
				&& ( !a->isTestable() || (a->isTestable() && a->getTestResult() != TmUndef)) ;
			)
			RHS (
				DBGR(a, "Parent Unresolved (one of dependencies Unresolved)!") ;
				a->setStatus( Component::UNRESOLVED ) ;
				a->setCommand( Component::Command::None ) ;
				) ) 

// testing complete, local result Failed
DEFRULE ( local_test_failed, 0, ComponentRef, a,
			LHS ( !a->dependencies().empty() &&
				std::none_of( a->dependencies().begin(), a->dependencies().end(),
 					[](auto i) { return i->getStatus() == Component::INPROGRESS || i->getStatus() == Component::UNDEFINED ; }) 
				&& a->getCommand() == Component::Command::Test 
				&& a->isTestable() && a->getTestResult() == TmFail ;
			)
			RHS (
				DBGR(a, "Parent Failed (local test)!") ;
				a->setStatus(Component::FAILED) ;
				a->setCommand( Component::Command::None ) ;
				) )

// testing complete, local result Unresolved
DEFRULE ( local_test_unresolved, 0, ComponentRef, a,
			LHS ( !a->dependencies().empty()
				&&
				std::none_of( a->dependencies().begin(), a->dependencies().end(),
 					[](auto i) { return i->getStatus() == Component::INPROGRESS || i->getStatus() == Component::UNDEFINED ; }) 
				&& a->getCommand() == Component::Command::Test 
				&& a->isTestable() && ( a->getTestResult() == TmUnresolved || a->getTestResult() == TmUntested )  ;
			)
			RHS (
				DBGR(a, "Parent Unresolved (local test)!") ;
				a->setStatus(Component::UNRESOLVED) ;
				a->setCommand( Component::Command::None ) ;
				) ) 

// test follow-ups
DEFRULE ( test_follow_up, 0, ComponentRef, a,
			LHS( !a->follow_ups().empty(); )

			RHS(
				DBGR(a, "Found follow-up tests to be started" )
				std::for_each(a->follow_ups().begin(), a->follow_ups().end(), [=](auto i)
								{
								if ( i->getStatus() == Component::UNDEFINED &&
								     i->getCommand() == Component::Command::None )
									{
				DBGR(a, "Testing follow-up: " + i->key() )
									i->setTestResult(daq::tmgr::TestResult::TmUndef) ;
									i->setCommand( Component::Command::Test ) ;
									}
								else
									{
				DBGR(a, "To follow-up test component is already tested: " << i->getStatus() )
									
									}
								}
							) ;
			) 
		)
/*
void
Component::load_rules()
{
Engine::instance().add_rule(&one_child_failed) ;
Engine::instance().add_rule(&fire_on_update) ;
Engine::instance().add_rule(&print_passed) ;
Engine::instance().add_rule(&print_failed) ;
Engine::instance().add_rule(&all_dependencies_passed) ;
} */
