#include <chrono>

#include <ers/ers.h>

#include <dvs/Rule.h>
#include <dvs/Engine.h>

using namespace daq::dvs ;
using namespace std::chrono ;

Engine& Engine::instance()
	{
	static Engine instance ;
	return instance ;
	} 

	// static destruction, 
Engine::~Engine() {
	if ( !m_exit ) { shutdown() ; }
}

void Engine::shutdown() // explicit destrcutor, called from ~dvsmanager
	{
	m_exit = true ;
	auto t = duration_cast<microseconds>(system_clock::now().time_since_epoch()).count() ;
	m_events.push(t) ; // unblock thread waiting on queue
	m_thread.join() ; // wait for thread to exit
	std::lock_guard lock(m_mtx) ;
	m_rules.clear() ;
	m_objects.clear() ;
	ERS_DEBUG(1, "Engine destructed") ;
	} 

void Engine::add_rule(RuleBase* r)
	{
	m_rules.push_back(r) ;
	ERS_DEBUG(1, "Registered rule " << r->get_id() ) ;
	std::sort(m_rules.begin(), m_rules.end(), [](const RuleBase* r1, const RuleBase* r2) { return r1->get_severity() > r2->get_severity() ; } ) ;
	}

void Engine::register_object(ReactorRef obj)
	{
	std::lock_guard lock(m_mtx) ;
	m_objects.insert(obj) ;
	ERS_DEBUG(3, "Registered object " << obj) ;
	}

void Engine::unregister_object(ReactorRef obj)
	{
	std::lock_guard lock(m_mtx) ;
	m_objects.erase(obj) ;
	ERS_DEBUG(3, "Unregistered object " << obj) ;
	}

// an object and it's update code
void Engine::add_event(ReactorRef obj, Callable clb)
	{
	std::lock_guard lock(m_mtx) ;
	// m_events_callable.emplace_back(system_clock::now(), obj, clb) ;
	ERS_DEBUG(5, "Callable event registered" ) ;
	}

// can be called from objects accessors
void Engine::add_event(const TimestampT& ts)
	{
	// std::lock_guard lock(m_mtx) ;
	if ( m_pause || m_exit ) return ;
	m_events.push(ts) ; // thread-safe
	ERS_DEBUG(4, "TS event registered: " << ts /*duration_cast<seconds>(ts.time_since_epoch()).count() */ << std::endl ) ;
	}

void Engine::stop()
	{
	m_pause = true ;
	//std::lock_guard lock(m_mtx) ;
	//m_events.clear() ; // this blocks the thread until new event arrives
	decltype(m_events)::value_type v ;
	while ( m_events.try_pop(v) ) ;
	ERS_DEBUG(2, "Engine paused" ) ;
	// m_events.push(system_clock::now()) ; // just to unblock thread waiting on the queue
	}

void Engine::resume()
	{
	m_pause = false ;
	ERS_DEBUG(2, "Engine resumed" ) ;
	}

Engine::Engine(): m_thread(&Engine::runloop, this)
	{
	ERS_DEBUG(1, "Engine constrcuted" ) ;
	}


// activated by particular event registered at event_time
void Engine::apply_all(const TimestampT& event_time)
	{
	ERS_DEBUG(4, "Checking event timestamp " << event_time) ;
	for ( auto o: m_objects )
		{
		ERS_DEBUG(5, "Looking for updated object " << o ) ;
		if ( o->get_state_timestamp() == event_time ) // apply for those object which were updated by this event
			{
			ERS_DEBUG(3, "Evaluating rules for updated object " << o ) ;
			for ( auto r: m_rules )
				{
				if ( r->evaluate_and_apply(o) )
					{
					ERS_DEBUG(2, "Rule " << r->get_id() << " applied for updated object " << o ) ;
					// return ; // apply only one rule from the top of the agenda -> other rules may be invalidated?
					}
				} ; // TODO ? separate async queue for executions
			}
		}
	}

void Engine::runloop()
	{
	while ( !m_exit )
		{
		// process events
/*		decltype (m_events) copy ;
		 { 	std::lock_guard lock(m_mtx) ;
			copy = m_events ;
			m_events.clear() ;
			}  */

/*		for ( auto [ ts, obj, call ] : m_events_callable )
			{
			call() ; // update objects (call set_XXX method)
			obj->set_timestamp(ts) ;
			apply_all(ts) ;

			}*/

		decltype(m_events)::value_type ev ;
	ERS_DEBUG(4, "Waiting for new event to process") ;
		m_events.pop(ev) ; // blocks until new event arrives in the queue
	ERS_DEBUG(4, "Got new event") ;
		if ( m_pause ) continue ; 
		if ( m_exit ) { ERS_DEBUG(2, "Exiting Engine thread") ; return ; }

		apply_all(ev) ; // apply all rules relevant for this event
	ERS_DEBUG(1, "Processed the event") ;

//		m_events_callable.clear() ; 
//		m_events.clear() ; 
		// std::this_thread::yield();
		// std::this_thread::sleep_for(1ms);	
		}
	ERS_DEBUG(1, "Exiting Engine thread") ;
	}
