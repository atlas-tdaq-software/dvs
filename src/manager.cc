#include <TM/Client.h>
#include <dvs/manager.h>
#include <dvs/dvsmanager.h>

namespace daq {
namespace dvs {

Manager::Manager() // throw
:m_tscope("any"), m_tlevel(255)
	{
	m_dvsm = new DVSManager() ;
ERS_DEBUG(1, "DVSManager constructed") ;
	}

Manager::~Manager()
	{
	delete m_dvsm ;
ERS_DEBUG(3, "DVSManager destructed") ;
	}


// callback from CLIPS
// obj_id: object which state is changed
// enum Status { Untested = 0, Failed, Passed, Unresolved, Inprogress, Notdone, Timeout, Nontestable } ;

/*
void Manager::IntReactor::status(Component::Status status, const char* obj_id)
{

if ( status == Component::Inprogress ) return ; // ignore for optimization, since it is useless anyway for client

ERS_DEBUG(4, "IntReactor status callback " << obj_id << " status: " << status) ;
Component* comp = manager_->getConfiguration()->findComponent(std::string(obj_id)) ;

if ( comp == nullptr )
	{
	ERS_LOG("Component " << obj_id << " not found in CLIPS status callback, this is not possible!") ;
	return ;
	}

ERS_DEBUG(4, "Calling Component setStatus on " << obj_id << " status: " << status) ;
comp->setStatus(status, nullptr) ; // initialize fields or set Result for composite
if ( g_callback!=nullptr ) { g_callback(comp->getResult()) ; }

}
*/

// throw CanNotLoadConfiguration
std::shared_ptr<Component>
Manager::loadConfiguration(Configuration& configuration, const std::string& partition, const std::string& segmentID, bool recursive, bool skip_apps)
{
	m_dvsm->loadDatabase(&configuration, partition, segmentID, recursive, skip_apps) ; // throw (CannotLoadDatabase )
ERS_DEBUG(1, "Database loaded") ;
	return m_dvsm->getConfiguration() ;
}

// throw CanNotReloadConfiguration
std::shared_ptr<Component>
Manager::reloadConfiguration(bool skip_apps)
{
	m_dvsm->reloadDatabase(skip_apps) ;  // this finally deletes dvs_configuration
ERS_DEBUG(1, "Database reloaded") ;
	return m_dvsm->getConfiguration() ;
}

std::pair<TestScope, TestLevel>
Manager::setTestScopeLevel(const TestScope& scope, TestLevel level)
{
	std::pair<TestScope, TestLevel> ret(m_tscope, m_tlevel) ;
	m_tlevel = level ;
	m_tscope = scope ;
	return ret ;
}

bool
Manager::setTestsAutoExec(bool yesno)
{
// TODO pass this to Engine
return true ;
}

TestsSet
Manager::getTestsForComponent(const Component* component)
{
return DVSManager::getTestsForComponent(component, m_tscope, m_tlevel) ;
}

void DummyCallback(Result tr)
{
}

// single test by ID, sync result
Result
Manager::testComponent(Component* component, const std::string& testID, TestScope scope, TestLevel level)
{
std::tuple<std::future<Result>, bool> ret = component->setCallback( daq::dvs::Callback(&DummyCallback) ) ;

component->setTestLevel(level) ;
component->setTestScope(scope) ;
component->setStatus(Component::UNDEFINED) ;
component->setTestResult(daq::tmgr::TestResult::TmUndef) ;
component->setTestById(testID) ;
component->setCommand(Component::Command::TestById) ;

ERS_DEBUG(4, "Waiting for test result of " << component->key()) ;

std::get<0>(ret).get() ; // block waiting for the result

ERS_DEBUG(4, "Test result for " << component->key() << " acquired, unblocking the client") ;

return component->getResult() ;
}

std::future<Result>
Manager::testComponent(Component* component, Callback callbk, TestScope scope, TestLevel level)
{
auto [future, ret] = component->setCallback( callbk ) ;

component->setTestLevel(level) ;
component->setTestScope(scope) ;
component->setStatus(Component::UNDEFINED) ;
component->setTestResult(daq::tmgr::TestResult::TmUndef) ;
component->setTestById("") ;
component->setCommand(Component::Command::Test) ;

return std::move(future) ;
}

std::future<Result>
Manager::testComponent(Component* component, CallbackV callbk, TestScope scope, TestLevel level)
{
auto [future, ret] = component->setCallback( callbk ) ; // init promise and result, fulfill old future, get the fresh future
// if testing is ongoing, do nothing, just return fresh future (which will also be fulfilled)
if ( ret ) { return std::move(future) ; }

component->setTestLevel(level) ;
component->setTestScope(scope) ;
component->setStatus(Component::UNDEFINED) ;
component->setTestResult(daq::tmgr::TestResult::TmUndef) ;
component->setTestById("") ;
component->setCommand(Component::Command::Test) ;

return std::move(future) ;
}

// backup_hosts is a list of child components of this application which results need to be ignored in the global result, or the tests not executed at all
std::future<Result>
Manager::testApplication(Component* application, const std::set<std::string>& backup_hosts, CallbackV callbk, TestScope scope, TestLevel level) 
{
	// init promise and result, fulfill old future if needed, get the fresh future
auto [future, ret]  = application->setCallback( callbk ) ;
// if testing is ongoing, do nothing, just return fresh future (which will also be fulfilled)
if ( ret ) { return std::move(future) ; }

// backup hosts are not direct children of an application: they are linked via intermediate BackupHosts node
// 
//application->addBackupHosts(backup_hosts) ; // TODO error handling
for ( auto dep: application->dependencies() )
	{
		// TODO change to string::ends_with() in C++20
		// used later from the rules to ignore failed results of backup hosts
		if ( dep->key().find(":host()") || dep->key().find(":backup_hosts()") )
			{
			dep->setBackupHosts(backup_hosts) ;
			}
	}

application->setTestLevel(level) ;
application->setTestScope(scope) ;
application->setStatus(Component::UNDEFINED) ;
application->setTestResult(daq::tmgr::TestResult::TmUndef) ;
application->setTestById("") ;
application->setCommand(Component::Command::Test) ;

return std::move(future) ;
}

void
Manager::stopTest(Component* component)
{
ERS_DEBUG(2, "Sending STOP test command to " << component->key()) ;
component->setCommand(Component::Command::Stop)  ;
}

void
Manager::resetTestResult(Component* component)
{
ERS_DEBUG(2, "Sending RESET command to " << component->key()) ;
// reset only TM result
component->resetComponent(true) ;
}

void
Manager::resetComponent(Component* component)
{
ERS_DEBUG(2, "Sending RESET command to " << component->key()) ;
// full DVS reset
component->resetComponent(false) ;
}

Component* Manager::findComponent(const std::string& id) 
{
return DVSManager::findComponent(id) ;
}


std::pair<const std::string, const std::string>
Manager::getTestLogs(const Component* component)
{
if ( component != nullptr )
	{
	auto th = component->getTestHandle().lock() ;
	if ( th )
		{
		ERS_LOG("Getting runtime logs for " << component->key()) ;
		// return std::make_pair<const std::string, const std::string>(th->getRuntimeStdout(),th->getRuntimeStderr()) ;
		// TODO
		return {"Not implemented","Not implemented"};
		}
	else
		{
ERS_LOG("getTestLogs: NULL test handle retrieved for " << component->key()) ;
		}
	}
ERS_LOG("Failed to get test runtime logs") ;
return {"",""} ;
}

const char * 
Manager::get_InitialLogRoot()
{
return m_dvsm->get_InitialLogRoot() ;
} 

const char *
Manager::get_PartitionLogRoot()
{
return m_dvsm->get_PartitionLogRoot() ;
} 

void Manager::setTestVerbosity ( bool flag )
{
DVSManager::getTestManager()->setVerbose( flag ) ;
} 

void Manager::discardTestOutput( bool flag ) 
{
DVSManager::getTestManager()->setDiscardLogs( flag ) ;
} 


} } // daq::dvs

