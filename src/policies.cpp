#include <boost/range/adaptor/reversed.hpp>

#include <config/Schema.h> 
 
#include <dal/ComputerBase.h>
#include <dal/HW_Object.h>
#include <dal/Component.h>

#include <TestManager/TestBehavior.h>
#include <dvs/dvsmanager.h> 

using namespace daq::config ;
using namespace daq::dvs ;

// returns vector of ConfigObjects following a relationship by string (e.g. Computer->Interfaces->vector<NIC>)
std::vector<const DalObject*>
DVSManager::getDependencies(const DalObject* wc_object, const std::string& relationship)
{
std::vector<const DalObject*> ret ;
// avoid loading disabled (e.g. Segments) objects
const daq::core::Component* comp = wc_object->cast<daq::core::Component>() ;
if ( comp )
	{
	if ( comp->disabled(*partition_) ) 
		{
		ERS_DEBUG(4, "Component " << wc_object->UID() << " is disabled, not loading TPs") ;
		return ret ; 
		}
	ERS_DEBUG(5, "Component " << wc_object->UID() << " is enabled" ) ;
	}

const daq::core::HW_Object* hwobj = wc_object->cast<daq::core::HW_Object>() ;
if ( hwobj )
	{
	if ( hwobj->get_State() == false ) 
		{
		ERS_DEBUG(4, "HW_object " << wc_object->UID() << " is Off, not loading TPs") ;
		return ret ; 
		}
	}

ERS_DEBUG(4, "Looking for DAL objects following relationship " << relationship << " in object " << wc_object->class_name() << "@" << wc_object->UID()) ;
return wc_object->get(relationship) ;
}


// load policies for a Component created from ConfigObject
std::list<std::pair<const daq::tm::TestBehavior*, std::vector<const DalObject*>>>
DVSManager::loadByTestPolicies(const DalObject* cobject)
{

std::list<std::pair<const daq::tm::TestBehavior*, std::vector<const DalObject*>>> ret ; // to be returned
ERS_DEBUG(3, "Looking for TP for object: " << cobject->UID()) ;

std::list<const daq::tm::TestPolicy*> policies ; // either by objId or by class_name

// first collect all defined policies for this objects 
getPolicies4Object(policies, cobject) ;

// for each policy, loop throuh its relation sequences and find related objects 
for ( const auto& policy: policies )
	{
	for ( const auto& tprel: policy->get_RelationsSequence() )
		{
		ERS_DEBUG(4, "Getting TP children following " << cobject->class_name() << "->" << tprel->get_RelationshipName()) ;

		// for multy-level TBs like Segments.IsControlledBy or Segment.Infrastrcurture, need to first get list of
		std::size_t pos = tprel->get_RelationshipName().find('.') ;
		
		if ( pos != std::string::npos ) // e.g. Segments.IsControlledBy or Segment.Infrastrcurture
			{
			std::string firts_level_rel = tprel->get_RelationshipName().substr(0,pos) ;
			std::string second_level_rel = tprel->get_RelationshipName().substr(pos+1) ;
			ERS_DEBUG(4, "Getting first level TP children following " << cobject->class_name() << "->" << firts_level_rel) ;
			std::vector<const DalObject*> intermediate_nodes = getDependencies(cobject, firts_level_rel) ;
			std::vector<const DalObject*> final_nodes ; // TODO std::list
			for ( const auto& int_obj: intermediate_nodes )
				{
				ERS_DEBUG(4, "Getting second level TP children following " << int_obj->class_name() << "->" << second_level_rel) ;
				auto objs = getDependencies(int_obj, second_level_rel) ;
				final_nodes.insert(final_nodes.end(), objs.begin(), objs.end()) ;
				}
			ret.emplace_back(tprel, final_nodes) ;
			}
		else
			{
			ret.emplace_back(tprel, getDependencies(cobject, tprel->get_RelationshipName())) ;
			}
		}
	}

ERS_DEBUG(3, "Found in total " << ret.size() << " child TOs for " << cobject->UID()) ;

// remove objects of classes which are prohibited by TestBehavior->IgnoreClasses list
for ( auto& pair: ret )
	{
	const auto& classes = pair.first->get_IgnoreClasses() ;
	if ( classes.empty() ) continue ;
	for ( auto itdalobj = pair.second.begin(); itdalobj != pair.second.end() ; )
		{
		for ( const auto& aclass: classes )
			if ( database_->try_cast(aclass, (*itdalobj)->class_name()) )
				itdalobj = pair.second.erase(itdalobj) ;
			else
				++itdalobj ;
		}
	}

return ret ;
}

void
DVSManager::getPolicies4Object( std::list<const daq::tm::TestPolicy*>& policies, const DalObject* cobject)
{
// first look into Policy4Object
auto it = s_test_policies_object.find(cobject->UID()) ;
if ( it != s_test_policies_object.end() )
	{
	ERS_DEBUG(4, "Found TP for object " << cobject->UID());
	policies.emplace_back(it->second) ;
	}
else
	{
	getPolicies4Class(policies, cobject->class_name()) ;
	}

ERS_DEBUG(3, "Found " << policies.size() << " TPs for " << cobject->UID()) ;
}

void
DVSManager::getPolicies4Class(std::list<const daq::tm::TestPolicy*>& policies, const std::string& class_name)
{
ERS_DEBUG(5, "Looking for TP in class: " << class_name) ;
auto it = s_test_policies_class.find(class_name) ;
if ( it != s_test_policies_class.end() )
	{
	ERS_DEBUG(4, "Found class TP for class " << class_name << ": " << it->second->UID());
	policies.emplace_back(it->second) ;
	return ; // FIXME Do we want to override base class TPs or to append ? Yes we do.
	}	

ERS_DEBUG(5, "Looking for TP in superclasses of " << class_name) ;
for ( const auto& sup_class: database_->get_class_info(class_name, true).p_superclasses )
	{
	getPolicies4Class(policies, sup_class) ;
	}

ERS_DEBUG(4, "Found " << policies.size() << " class TPs for " << class_name) ;
}
