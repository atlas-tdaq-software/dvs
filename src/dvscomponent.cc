#include <iostream>
#include <unistd.h>
#include <algorithm>

#include <ers/ers.h>
#include <config/DalObject.h>
#include <config/Configuration.h>

#include <dal/Detector.h>
#include <dal/Crate.h>
#include <dal/Module.h>
#include <dal/Segment.h>
#include <DFdal/HW_InputChannel.h>
#include <dal/Interface.h>
#include <TM/Client.h>

#include <dvs/dvscomponent.h>
#include <dvs/dvsenvironment.h>
#include <dvs/dvsmanager.h>

/*
namespace daq {

ERS_DECLARE_ISSUE(      dvs, 
                        UnexpectedTestResult, 
                        message << component, 
                        ((std::string)message)  
                        ((std::string)component) ) 

} */

using namespace daq::dvs ;

// static callback shared by Components in the same session
Callback Component::s_callback = nullptr ;
	
std::ostream& daq::dvs::operator<< (std::ostream& strm, const Component* comp)
{  
   if ( !comp ) return strm ; 
   static size_t level = 0 ;
   
   level++;
   strm << comp->key() << std::endl;

   for ( auto child: comp->dependencies() )
   {
   	for ( size_t i = 0 ; i < level ; i++ )
		{ strm << "\t" ; }
	strm << child ;
   }
   
   level--;
   return strm;
}

Component::Component(	const std::string& name, Type type, const DalObject* object )
{
ERS_DEBUG(4, "Constucting Component " <<  name) ;
	name_ = name ;
	type_ = type ;
	m_callback_v = nullptr ;
	testable_object_ = nullptr ;
	dal_object_ = nullptr ;
	m_is_promised = false ;
	sync_policy_ = false ;
	stop_on_error_ = false ;
	parent_test_order_ = First ;
	m_command = Command::None ;
	is_testable_ = false ;

	// create some unique identifier
	if ( object )
		{
ERS_DEBUG(5, "Constructing Component from DalObject " << object->class_name() << "@" << object->UID() ) ;
		class_name_ = object->class_name() ;
		key_ = name ; // uniquie ID for benefit of RC to find Applications
		obj_uid_ = object->UID() ;
		dal_object_ = object ;
		testable_object_ = object->cast<daq::core::TestableObject>() ;
		if ( testable_object_ )
			{
			is_testable_ = true; 
ERS_DEBUG(6, "Constructing Component " << testable_object_->UID() << "(" << class_name_ << ") as TestableObject") ;
			}

		if ( const daq::core::HW_Object* obj = object->cast<daq::core::HW_Object>() )
			{
ERS_DEBUG(6, "Constructing Component " << obj->UID() << "(" << class_name_ << ") as HW_Object") ;
			key_ = name_ = /*class_name_ + "@" + */ obj->UID() ;
			help_link_ = obj->get_HelpLink() ;
			}
		}
	else
		{		
		key_ = name_ ;
		class_name_ = "" ;
		obj_uid_ = "" ;
		}
	
	if ( object )
		{
		std::list<const daq::tm::TestPolicy*> policies ;
		DVSManager::getPolicies4Object(policies, object) ;

		bool sync = false ;
		bool stop_on_error = false ;
		auto test_container = daq::tm::TestPolicy::TestOfContainer::First ;
		bool set = false ;		

		for ( auto policy: policies )
			{
ERS_DEBUG(4, "Loading Test policy " << policy->UID() << " for " << key_); 
			if ( set && (sync != policy->get_SynchronousTesting()
						|| stop_on_error != policy->get_StopOnError()
						|| test_container != policy->get_TestOfContainer()) )
				{ ers::warning( BadTestPolicy(ERS_HERE, policy->UID(), "Conflicting multiple Test Policies found for object " + object->UID())) ; }

			sync = policy->get_SynchronousTesting() ;
			stop_on_error = policy->get_StopOnError() ;
			test_container = policy->get_TestOfContainer() ;
			set = true ;
			} ;

		sync_policy_ = sync ;
		stop_on_error_ = stop_on_error ;
		parent_test_order_ = ( test_container == daq::tm::TestPolicy::TestOfContainer::Last ? Last : First ) ;
		}

	// initial values for test results
	m_result.globalResult = daq::tmgr::TestResult::TmUndef ;
	m_result.componentResult.result = daq::tmgr::TestResult::TmUndef ;
	m_result.componentId = key_ ;
	m_status = Component::UNDEFINED ;
}

// constructor for TB containers, where display name need to be cooked a bit
Component::Component( const std::string& name, const daq::tm::TestBehavior& beh )
:Component(name + ":" + beh.get_RelationshipName(), CompositeEntity)
{
name_ = beh.get_RelationshipName() ;
if ( name_.find("()") != std::string::npos )
	{
	name_.resize(name_.size()-2) ; // cut off trailing () as part of algorithm name
	}

sync_policy_ = beh.get_SynchronousTesting() ;
stop_on_error_ = beh.get_StopOnError() ;
}

/** Destructor for parent object. Destruct here all children.
*/
Component::~Component()
{
ERS_DEBUG(2, "Destructing  " << key() ) ;
	// now it is done globally in ~DVSManager, see all_components_ collection
	for ( auto child: children_ ) { delete child ; }
	children_.clear() ;
	deps_.clear() ;
	follow_ups_.clear() ;

std::lock_guard<std::mutex> ml(m_update_mutex);
if ( m_is_promised )
	{
	ERS_DEBUG(1, "Component " << key() << " is being deleted while the future is promised. Shall I fulfill the promise here?") ;
	m_promised_result.set_value(m_result) ;
	}

ERS_DEBUG(3, "Destructed  " << key() ) ;

}

/**
 * Sets current levelk'scope for this component and its deps
 */
void Component::setTestLevel(int level) 
{
	m_test_level = level ;
	for ( auto c: dependencies() ) 
		{
		c->setTestLevel(level) ;
		}
}

int Component::getTestLevel() const 
{
	return m_test_level ;
}

void Component::setTestScope(const std::string& scope) 
{
	m_test_scope = scope ;
	for ( auto c: dependencies() ) 
		{
		c->setTestScope(scope) ;
		}
}

std::string Component::getTestScope() const 
{
	return m_test_scope ;
}

/**
	if the comp already exists, delete it and replace the pointer
	with existing one
*/
bool Component::addChild( Component* comp, const daq::tm::TestBehavior* )
{
// prevent duplicate components which may be a lot coming from omitting SW nodes ?
   if ( std::find(children_.begin(), children_.end(), comp) != children_.end() ) // component already inserted
		{
ERS_DEBUG(0, "Must not be here: " << comp->key() << " is already contained in " << key() ) ;
		return false ;
		} 

   children_.push_back(comp) ;
ERS_DEBUG(4, "Added child " << comp->key() << " to " << key() ) ;

   addDeps(comp) ;

   return true ;
}	

void Component::addDeps( Component* comp ) 
{
//if ( children_.find(comp->key()) != children_.end() ) { return ; }
//if ( std::find(deps_.begin(), deps_.end(), comp ) == deps_.end() ) { return ; }
//deps_.push_back(comp) ;
deps_.push_back(comp) ;
ERS_DEBUG(4, "Added dependence " << comp->key() << " to " << key() ) ;
}

void
Component::get_dependencies(std::vector<ReactorRef>& deps) const
	{
	// for ( const auto& ch: deps_ ) { deps.push_back(ch) ; } ;
	std::copy(deps_.begin(), deps_.end(), std::back_inserter(deps)) ;
//	std::lock_guard<std::mutex> ml(m_update_mutex) ;
//	for ( const auto& host: backup_hosts_ ) { deps.push_back(host) ; } ;
ERS_DEBUG(4, "Returning dependencies of " << key() ) ;
	}

// recursively get a vector of results from all subtree, to be published by RC in IS
void Component::getTestResults(std::vector<Result>& results)
{
 { std::lock_guard<std::mutex> ml(m_update_mutex); // m_result can be updated from another thread e.g. for another parent application
    if ( m_result.componentResult.test_results.size() != 0 ) // do not return results with no tests, e.g. for TP containers
		{ results.push_back(m_result) ; }
 }

 for ( auto child: deps_ ) 	{ child->getTestResults(results) ; }

 // TODO add follow-ups?
}


// callback with change of Status, when global testing (e.g. of a composite) is completed
// calls user callback or fulfill promise if defined (i.e. this is the top component which was tested)
void Component::setStatus(Status status)
{
ERS_DEBUG(2, "setStatus callback: " << key_ << ": " <<  status ) ;
std::lock_guard<std::mutex> ml(m_update_mutex);
m_status = status ;

if ( status == UNDEFINED )
	{	
ERS_DEBUG(4,"Initializing Result for component " << key_ ) ;
		m_diagnosis = "" ;
		m_result.actions.clear() ;
		m_result.globalResult = daq::tmgr::TestResult::TmUndef ;
		m_result.componentResult.result = daq::tmgr::TestResult::TmUndef ;
		send_event() ;
		
		if ( status == UNDEFINED && s_callback != nullptr ) // e.g. reset was done after a test
			{
			ERS_DEBUG(2, "Calling user callback for Undefined") ;
			s_callback(m_result) ;
			}
		return ; 
	}

if ( status == INPROGRESS ) { send_event() ; return ; }

m_result.globalResult = (daq::tmgr::TestResult)status ; // from Status to tmrg::TestResult
send_event() ;

ERS_DEBUG(1, "Global test status of " << key()<< " is defined: " << m_result.globalResult ) ;

// here status is determined, call user callback for case of vector result
if ( m_callback_v != nullptr )
	{
	m_results.push_back(m_result) ; // always add first 

// NB: this can be exactly the case if policy is not to start local (container) test when child tests failed in sync mode
//	if ( m_result.testResult.result == daq::tmgr::TestResult::TmUndef ) 
//		{
//		ers::warning(UnexpectedTestResult(ERS_HERE, "Unexpected: local test result is TmUndef for application ", key_)) ;
//		}

	// recursively get all results from children
	for ( auto child: deps_ ) 	{ child->getTestResults(m_results) ; }

	ERS_DEBUG(2, "Calling user callbackV for component: " << key() << " with status: " << m_result.globalResult <<
	" and " << m_results.size() << " dependent component test results") ;

	m_callback_v( m_results ) ; 	// return vector of children results + global
	}
else
if ( s_callback != nullptr ) // TODO thread safe?
	{
	ERS_DEBUG(2, "Calling user callback for component: " << key() << " with status: " << m_result.globalResult ) ;
	s_callback(m_result) ;
	}

if ( m_is_promised )
	{ 
	try {  
ERS_DEBUG(2, "Component " << key() << " fullfilled promised result " << m_result.globalResult ) ;
		m_promised_result.set_value( m_result ) ; // TODO set diagnostics
		m_is_promised = false ;
	}
	catch ( const std::future_error& ex ) // shall not be here
		{
		std::cerr << "UNEXPECTED (FILE A BUG): m_promised_result already set for component " << key_ << ":" << ex.what() <<std::endl ; 
		}
	}

clearTestFollowUp() ; 
}


std::tuple<std::future<Result>, bool>
Component::setCallback( Callback cb )
{
std::lock_guard<std::mutex> ml(m_update_mutex);

m_result.globalResult = daq::tmgr::TestResult::TmUndef ;
m_result.componentResult.result = daq::tmgr::TestResult::TmUndef ;
m_result.actions.clear() ;
bool ret = false ; // return true if the testing is already ongoing and future result promised

if ( m_is_promised )
	{
	try { // before reseting the future, fulfill the possible future to avoid problems in client code using that future
ERS_DEBUG(1, "Component " << key() << " is being tested while new test is requested, fulfilling older future") ;
		m_promised_result.set_value(m_result) ;  
		ret = true ;
		}
	catch ( std::future_error& ex )
		{
		std::cerr << "UNEXPECTED: m_promised_result.set_value(TmUndef) exception: " << ex.what() << std::endl ;
		}
	}

s_callback = cb ;
m_callback_v = nullptr ;
m_promised_result = std::promise<Result>() ; // reset promise, old one is still OK: the value is set
m_is_promised = true ;
ERS_DEBUG(2, "Component " << key() << " set callback ") ;
return std::make_tuple(m_promised_result.get_future(), ret) ;
} 

std::tuple<std::future<Result>, bool>
Component::setCallback( CallbackV cb )
{
std::lock_guard<std::mutex> ml(m_update_mutex);

m_result.globalResult = daq::tmgr::TestResult::TmUndef ;
m_result.componentResult.result = daq::tmgr::TestResult::TmUndef ;
m_result.actions.clear() ;
bool ret = false ; // return true if the testing is already ongoing and future result promised

if ( m_is_promised )
	{
	try { // before reseting the future, fulfill the already waiting future to avoid problems in client code using that future
		m_promised_result.set_value(m_result) ; // m_result is TmUndef
		ret = true ;
		}
	catch ( std::future_error& ex )  // shall not be here
		{
		std::cerr << "UNEXPECTED: m_promised_result.set_value(TmUndef) exception: " << ex.what() << std::endl ;
		}
	}

m_callback_v = cb ;
s_callback = nullptr ;
m_results.clear() ;

m_promised_result = std::promise<Result>() ; // reset promise waiting now for the new result, the existing future (if any) is fulfilled (with TmUndef)
m_is_promised = true ;
return std::make_tuple(m_promised_result.get_future(), ret) ; // fresh future for the client
} 

void Component::resetComponent(bool test_result_only)
{
	{
	const daq::core::TestableObject* to = testableObject() ;
	if ( to != nullptr )
		{
	ERS_DEBUG(2, "Resetting TM test status for TestableObject " << to->UID() );
		DVSManager::getTestManager()->reset(*to) ;
		}
	}

if ( !test_result_only ) // do not reset CLIPS data since the application and its Host may be under testing
	{
	ERS_DEBUG(2, "Resetting status for " << key_);
	setTestResult(daq::tmgr::TestResult::TmUndef) ;
	setStatus(UNDEFINED) ;
	}

//for ( auto it : children() )	{ it->resetComponent() ; }
for ( auto it : dependencies() )	{ it->resetComponent(test_result_only) ; }
for ( auto it : follow_ups() )		{ it->resetComponent(test_result_only) ; }
clearTestFollowUp() ; // as we cleared test results, related follow-ups are also invalidated
}

DVSApplication::DVSApplication( const daq::core::BaseApplication* app_cfg) : 
Component( app_cfg->UID(), Application, app_cfg ), host_(app_cfg->get_host()->UID()), app_cfg_(app_cfg)
{
}

void Component::setBackupHosts(const std::set<std::string>& bhosts)
{
std::lock_guard<std::mutex> ml(m_update_mutex);
backup_hosts_ = bhosts ;

// backup_hosts_.clear() ; 
/*
for ( const auto& hname: bhosts)
	{
		Component* Host = DVSManager::findComponent(hname) ;
		if ( Host == nullptr ) // something wrong passed by RC
			{
				// TODO? 
				std::cerr << "Loading backup hosts for application " << key() << ": failed to find Computer by name " << hname << std::endl ;
				continue ;
			}
		backup_hosts_.push_back(Host) ;
	}
*/
ERS_DEBUG(2, "Backup hosts size: " << backup_hosts_.size() ) ;

}
