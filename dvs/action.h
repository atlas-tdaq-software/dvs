#ifndef DVS_ACTION_H
#define DVS_ACTION_H

#include <string>

// DAL classes
#include <TestManager/Executable.h>
#include <TestManager/TestFailureAction.h>


namespace daq::dvs {

/**
 * \class FailureAction
 * 
 * \brief Helper structure which reflects dal::TestFailureAction but makes parameters substitution 
 * accordingly with the #this.RunsOn syntax for the tested component.
 * 
 *  command: enum {"Reboot", "Exec", "Test" ...}
 *  description: string
 *  paramaters_json: whatever is meaningful for particular command, encoded in JSON,
 *      e.g. for a Test: { "Component": "pc-tdq-onl-23.cern.ch"; "Test": "test_pmg_agent"; }
 *      e.g. for a Exec: { "Component": "pc-tdq-onl-23.cern.ch"; "Test": "test_pmg_agent"; }
 * 
 * \see dal::TestFailureAction
 */
class FailureAction
{
public:

std::string command ;     // just copy from TestFailureAction
std::string description ; // just copy from TestFailureAction
std::string parameters_json ;  // substituted from the tested component attributes
uint32_t timeout ; 
} ;


}
#endif