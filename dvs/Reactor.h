#ifndef DVS_REACTOR_H
#define DVS_REACTOR_H

#include <chrono>
#include <atomic>
// #include <mutex>
#include <list>
#include <vector>

namespace daq::dvs {

class Reactor ;

// may be shared_ptr
using ReactorRef = Reactor* ;
using TimestampT = unsigned long long ; // std::chrono::system_clock::time_point

// base class for user classes managed by Rule<C> and Engine
class Reactor
{
public:

// registers object in the engine
Reactor() ;

// sends event to the engine
void send_event() ;

TimestampT get_timestamp() const	        { return m_state_timestamp ; } ;
void set_timestamp( const TimestampT& tp )	{ m_state_timestamp = tp ; } ;

// non-copyable
Reactor& operator=(const Reactor&) = delete ;
Reactor(const Reactor&) = delete ;

virtual ~Reactor() ;

TimestampT get_state_timestamp() ;

virtual void
get_dependencies(std::vector<ReactorRef>&) const = 0 ;

// load user rules
//virtual void
//load_rules() const = 0 ;

protected:
std::vector<ReactorRef> m_deps ;

private:
std::atomic<TimestampT> m_state_timestamp ; // time of last update

// std::mutex m_mtx ; // protect the timestamp
} ;


}

#endif
