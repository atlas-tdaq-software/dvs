/**
 * \file dvs/manager.h
 *
 * \date Nov 26, 2013
 * \author: Andrei Kazarov
 * \copyright 2013-2023 CERN for the benefit of the ATLAS Collabioration 
 */

#include <functional>
#include <vector>
#include <set>

//#include <TM/Client.h>

#include <dvs/exceptions.h>
#include <dvs/dvscomponent.h>

#ifndef MANAGER_H_
#define MANAGER_H_

namespace daq {
namespace dvs {

// forward declaration, avoiding dependency on implementation dvsmanager.h
class DVSManager ;

// see https://its.cern.ch/jira/browse/ATDAQCCDVS-2?jql=project%20%3D%20ATDAQCCDVS

using TestScope = std::string ; /** Test scope as defined in Test core class. Better to get it from generated DAL? */
using TestLevel = int ; 		/**	Test level */

/**
 * One of 2 main public classes to DVS, entry point for users.
 * To be used with daq::dvs::Component class.
 *
 * Use pattern:
 * - create daq::dvs::Manager
 * - call loadConfiguration
 * - browse tree of Components and tests for Components
 * - select Component(s) to test
 * - launch relevant testComponent(s) method (provide callback)
 * - sync on result, process callbacks
 * 
 * @see Component
 */
class Manager
{
public:

	/**
	 * Constructor, initializes Rules engine
	 */
	Manager() ;

	~Manager() ;

	/**
	 * Loads into DVS a configuration for a segment in partition, recursively or not.
	 * Initializes TM client, builds up tree of testable components
	 *
	 * @param configuration user-constructed Configuration object
	 * @param partition		partition name
	 * @param segmentID		segment ID, if empty then load all segments (OnlineInfr will be next to other segments, not at the top)
	 * @param recursive		load subsegments (for RC should be false)
	 * @param skip_apps		do not load Applicaitons (for RC should be false)
	 *
	 * @return shared pointer to top-level Component of the testable tree. Use it to browse the tree, to find components by ID, and to test the components.
	 * @see Component::children() - get children
	 * @see Component::key() - get unique ID
	 * @see Manager::findComponent(const string& ID) - get component by ID (lookup performed among children of this Component)
	 * // Component presently typdefs to legacy DVSComponent
	 *
	 * @throw CannotLoadDatabase 	// encapsulates other exceptions, e.g. from dal, TM
	 */
	std::shared_ptr<Component> loadConfiguration(Configuration& configuration, const std::string& partition, const std::string& segmentID = "", bool recursive = true, bool skip_apps = false) ;


	/**
	 * To be called when database has changed
	 *
	 * Resets the shared_ptr<const Component*> returned in loadConfiguration.
	 * NB: User should not rely on pointers to components beyond lifetime of *loadConfiguration methods:
	 * all pointers to Components previously returned from loadConfiguration shall not be used.
	 *
	 * @throw CannotLoadDatabase 	// the same set of reasons as in loadConfiguration
	 */
	std::shared_ptr<Component>  reloadConfiguration(bool skip_apps = false) ;

	/**
	 * Sets new test scope and level (to be used unless it is not specified explicitly in testComponent)
	 *
	 * @param scope
	 * @param level
	 * @return pair of previously defined scope and level
	 */
	std::pair<TestScope, TestLevel> setTestScopeLevel(const TestScope& scope, TestLevel level) ;

	/**
	 * Instruct DVS to launch additional tests (for other components) as defined in Test::Failures::TestUponFailure
	 *
	 * The default behavior is false
	 *
	 * @param yesno if true, DVS will follow TestUponFailure actions automatically
	 * @return previous state of the flag
	 */
	bool 	setTestsAutoExec(bool yesno) ;

	/**
	 * For browsing tests configured for a Component at presently defined scope/level.
	 * This includes 'interactive' tests, formerly known in dvs gui as 'actions' - possible confusion...
	 *
	 * @param 	component	const pointer to Component from the tree returned in loadConfiguration
	 * @return	std::set<const daq::tm::Test*> collection of tests, may be empty
	 */
	TestsSet getTestsForComponent(const Component* component) ;

	/**
	 * @brief Synchronous testing of a single component with single test as TestID
	 *
	 * Executes a particular test on a component, returning synchronous single test result.
	 * To be used from DVS, allowing users to browse tests for components, select and run individual tests.
	 *
	 * @see getTestsForComponent()
	 *
	 * @param component	const pointer to Component from the tree returned in loadConfiguration
	 * @param testID	just a string ID of particular test on the Component. Can be get via getTestsForComponent().
	 * @param scope
	 * @param level
	 * @return 		standard TM result of the test(s) executed for the component
	 *
	 * @throw test not found etc...
	 */
	Result testComponent(Component* component, const std::string& testID, TestScope scope, TestLevel level) ;

	/**
	 * @brief Asynchronous testing of a single component, single-result callback
	 *
	 * Start async testing of a single component, with possible synchronization on future<GlobalResult>
	 * "DVSGUI-style" of testing (when Component can be a root of a tree)
	 * User callback is called per-component and for all (dependent) components which were tested - use daq::tm::Result::componentID to find the corresponding object
	 *
	 * @see daq::tm::Result::componentID
	 *
	 * @param component	pointer to Component from the tree returned in loadConfiguration
	 * @param callbk	Scalar callback, @see typedef Callback. Contains test result for a single component.
	 * @param scope
	 * @param level
	 * @return future of DVS Result - cumulative result + diagnosis. Calling get() on it will block until testing is completed.
	 *
	 * @throws TM exceptions (bad config etc)
	 */
	std::future<Result> testComponent(Component* component, Callback callbk, TestScope scope, TestLevel level) ;

	/**
	 * @brief Asynchronous testing of a single components, vector-component callback
	 *
	 * Start async testing of a components, with possible synchronization on future<GlobalResult>. Usually involves testing of other components,
	 * defined by the rules and policies.
	 * "RC-style" of testing, when Component is a leaf Application in a segment.
	 * User callback is called per-component and for all (dependent) components which were tested - use daq::tm::Result::componentID to find the corresponding object
	 *
	 * @see daq::tm::Result::componentID
	 *
	 * @param component	pointer to Component from the tree returned in loadConfiguration
	 * @param callbk	Vector-results callback, @see typedef CallbackV. Contains vector of test results for a multiple components.
	 * @param scope
	 * @param level
	 * @return future of DVS Result - final result + diagnosis. Calling get() on it will block until testing is completed.
	 *
	 * @throws TM exceptions (bad config etc)
	 */
	std::future<Result> testComponent(Component* component, CallbackV callbk, TestScope scope, TestLevel level) ;

	/**
	 * @brief Similar to testApplication(... host ... )
	 *
	 * But passes the DVS core the list of (backup) hosts which are meant NOT to be tested in the scope of this Application
	 * (or tests results must be ignored)
	 *
	 * @param application	pointer to Component from the tree returned in loadConfiguration
	 * @param backup_hosts	string names of the Backup Hosts where this application may run (but not it's real Host)
	 * @param callbk	Vector-results callback, @see typedef CallbackV. Contains vector of test results for a multiple components.
	 * @param scope
	 * @param level
	 * @return future of DVS Result - final result + diagnosis. Calling get() on it will block until testing is completed.
	 *
	 * @throws TM exceptions (bad config etc)
	 */
	std::future<Result> testApplication(Component* application, const std::set<std::string>& backup_hosts, CallbackV callbk, TestScope scope, TestLevel level) ;

	/**
	 * Interrupts testing of a Component. For a composite, sends stopTest to all child components.
	 *
	 * @param component pointer to a component.
	 * @throws NoTestHandle if no currently running tests found (handle=nullptr)
	 */
	void stopTest(Component* component) ;

	/**
	 * Clears testing result and sets STATUS=UNTESTED on a component and its dependencies.
	 * Used in DVS to clear the tests results.
	 * @warning Shall not be used from RC on a particular Application, in case some other Applications are being tested at the same time.
	 *
	 * @param component pointer to a component.
	 */
	void resetComponent(Component* component) ;

	/**
	 * Clears testing result of a component and its dependencies
	 *
	 * @param component pointer to a component.
	 */
	void resetTestResult(Component* component) ;

	/**
	 * Runtime access to tests output
	 *
	 * @param component
	 * @return pair of string corresponding to stdout & stderr streams of the tests
	 */
	std::pair<const std::string, const std::string> getTestLogs(const Component* component) ;

	/**
	 * Find a Component by ID
	 *
	 * @param id of a Component to find
	 * @return pointer to a DVS Component
	 */
	Component* findComponent(const std::string& id) ;

	/** returns LogRoot for initial partition */
	const char * get_InitialLogRoot() ;

	/** returns LogRoot for partition */
	const char * get_PartitionLogRoot() ;

	void	setTestVerbosity ( bool flag ) ;
	void 	discardTestOutput( bool flag ) ; 

	// internal stuff, implementation is presently based on legacy DVS
private:

	DVSManager*	m_dvsm ; // implementation manager

	std::shared_ptr<Component>	m_tree ;  // collection of all loaded Components, returned and shared with the client

	TestScope	m_tscope ; // currently selected test scope
	TestLevel	m_tlevel ; // currently selected test level
} ;

}}

#endif /* MANAGER_H_ */

