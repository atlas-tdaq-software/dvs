#ifndef DVS_ENGINE_H
#define DVS_ENGINE_H

#include <functional>
#include <thread>
#include <mutex>
#include <atomic>
#include <vector>
#include <unordered_set>
#include <chrono>

#include <tbb/concurrent_queue.h>

#include <dvs/Reactor.h>

using Callable = std::function<void()> ;

namespace daq::dvs {

class RuleBase ;

class Engine
{
// friend class Reactor ;

public: 

static Engine& instance() ;

void add_rule(RuleBase* r) ;
void register_object(ReactorRef obj) ;
void unregister_object(ReactorRef obj) ;

// an object and it's update code
void add_event(ReactorRef obj, Callable clb) ;

// can be called from objects accessors
void add_event(const TimestampT& ts) ;

// stop processing new events, blocks the thread - does not destruct
// to be called in dvsmanager destructor or before db reload
void stop() ;
// unblocks new event arrival, call after db reload
void resume() ;

Engine() ;

// static (singleton) destructor
~Engine() ;
void shutdown() ;

Engine(const Engine&) = delete ;
Engine& operator=(const Engine&) = delete ;

private:

std::vector<RuleBase*> m_rules ;
// std::deque<std::tuple<std::chrono::system_clock::time_point, ReactorRef, Callable>> m_events_callable ; // events 
tbb::concurrent_bounded_queue<TimestampT> m_events ; // events 
std::unordered_set<ReactorRef> m_objects ; // all objects
std::atomic<bool> m_pause = false ; // flag to not process new events
std::atomic<bool> m_exit = false ; // flag to exit from thread
std::thread m_thread;
std::mutex m_mtx ;

// activated by particular event registered at event_time
void apply_all(const TimestampT& event_time) ;

void runloop() ;

} ;

}

#endif
