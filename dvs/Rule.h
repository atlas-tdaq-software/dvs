#ifndef DVS_RULE_H
#define DVS_RULE_H


#include <functional>
#include <iostream>
#include <string>

#include <ers/ers.h>

#include <dvs/Engine.h>

namespace daq::dvs {

using Callable = std::function<void()> ;
using CallableBool = std::function<bool()> ;

// function that returns tuple<bool,callable> and called on a instance of T
template <class T>
using Predicate = std::function<std::tuple<bool, Callable>(T)> ;

// function that returns two Callables, the first of which returns bool
template <class T>
using Predicate2 = std::function<std::tuple<CallableBool, Callable>(T)> ;

// from Reacror.h
class Reactor ;
using ReactorRef = Reactor* ;

class RuleBase
{
public: 
RuleBase(const std::string id, int severity = 0):m_severity(severity), m_id(id)
	{
	Engine::instance().add_rule(this) ;
	ERS_DEBUG(0, "Added rule " << id) ;
	}
	
virtual ~RuleBase() {} 
int get_severity() const { return m_severity ; } 
const std::string& get_id() const { return m_id ; } 

virtual bool evaluate_and_apply(ReactorRef object) = 0 ;
virtual Callable evaluate(ReactorRef object) = 0 ;

protected:
int				m_severity = 0 ;
std::string		m_id ;

} ;

// T is user object
template <class T>
class Rule: public RuleBase
{
Predicate2<T> 	predicate ;

public:

Rule( const std::string& id, const Predicate2<T>& rs, int severity = 0 ) 
:RuleBase(id, severity), predicate(rs) {}

Rule( const Rule& other ): RuleBase(other.m_id, other.m_severity)
	{
	predicate = other.predicate ;
	}

Rule& operator=( const Rule& other ) = delete ;

int get_severity() const { return m_severity ; }

// returns true if the RHS was executed
bool evaluate_and_apply(ReactorRef object) 
	{
	T userobject = dynamic_cast<T>(object) ;
	if ( userobject == nullptr )
		{
		std::cerr << "Failed to downcast ReactorRef to user type" << std::endl;
		return false ; // TODO exception 
		}
	auto [ lhs, rhs ] = predicate(userobject) ;
	if ( lhs() )	// check LHS
		{
		ERS_DEBUG(2, "Rule " << m_id << " fired on object " << userobject->key()) ;
		rhs() ;  	// apply RHS 
		return true ;
		} ;
	return false ;
	}

Callable evaluate(ReactorRef object) 
	{
	T userobject = dynamic_cast<T>(object) ;
	if ( userobject == nullptr )
		{
		return []{ std::cerr << "Failed to downcast ReactorRef to user type" << std::endl; } ;
		// TODO exeption 
		}
	return std::get<Callable>(predicate(userobject)) ;
	}

} ;

#define DEFRULE(rulename, severity, userclass, capture, ...) \
Rule<userclass> rulename ( #rulename,  [](userclass capture) { return std::make_tuple( __VA_ARGS__ ) ;  }, severity ) ;

#define LHS(...) [=] { return __VA_ARGS__ }, 
#define RHS(...) [=] { __VA_ARGS__ } 

}

#endif
