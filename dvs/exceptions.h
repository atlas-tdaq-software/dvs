#ifndef DVS_EXCEPTIONS_H
#define DVS_EXCEPTIONS_H

#include <string>

#include <ers/Issue.h>

namespace daq {

ERS_DECLARE_ISSUE(      dvs, 
                        CannotOpenFile, 
                        message << file_name, 
                        ((std::string)message)  
                        ((std::string)file_name) ) 

ERS_DECLARE_ISSUE(      dvs, 
                        CannotLoadKB, 
                        "Can not load CLIPS code from file: " << file_name, 
                        ((std::string)file_name) ) 

ERS_DECLARE_ISSUE(      dvs, 
                        FailedSubscribe, 
                        "Failed to subscribe to database changes.", )

ERS_DECLARE_ISSUE(      dvs, 
                        CannotLoadDatabase, 
                        "Can not load configuration: " << message,
                        ((std::string)message) )

ERS_DECLARE_ISSUE(      dvs, 
                        CannotReloadDatabase, 
                        "Failed to reload database that was changed.", )


ERS_DECLARE_ISSUE(      dvs, 
                        BadCast, 
                        ,
 			((std::string)message) )

ERS_DECLARE_ISSUE(      dvs, 
                        CannotLoadInstance, 
                        "Can not create instance of " << object << ": "<< message,
 			((std::string)object)
 			((std::string)message) ) 

ERS_DECLARE_ISSUE(      dvs, 
                        CannotFindInstance, 
                        "Can not find CLIPS instance: " << object,
 			((std::string)object) ) 

ERS_DECLARE_ISSUE(      dvs, 
                        NoTestHandle,
			"No active test handle found for component", ) 

ERS_DECLARE_ISSUE(      dvs, 
                        BadTestPolicy,
			"Bad configuration of TestPolicy " << policy << ":" << reason,
 			((std::string)policy)
 			((std::string)reason) ) 

ERS_DECLARE_ISSUE(      dvs, 
                        BadAction,
			"Bad configuration of TestFailureAction " << action << ":" << reason,
 			((std::string)action)
 			((std::string)reason) ) 

} // daq

#endif
