#ifndef DVS_ENVIRONMENT_H
#define DVS_ENVIRONMENT_H

#include <string>

namespace daq {
namespace dvs {

class DVSEnvironment
{
public:
	static const std::string& kbRoot() ;
	static const std::string& execRoot()	;
	static const std::string& helpRoot()	;
	static const std::string& logsPath()	;
	static int	  	  maxTests()	;
	static const std::string& ipcRefFile()	;
private:
	static void initialize();
	
	static std::string	exec_root_;
	static std::string	kb_path_;
	static std::string	help_root_;
	static std::string	logs_path_;
	static std::string	ipc_ref_file_;
	static int		max_tests_ ;
	static bool		initialized_;	
};

}}  //namespace daq::dvs

#endif // DVS_ENVIRONMENT_H
