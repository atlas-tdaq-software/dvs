#ifndef DVS_MANAGER_H
#define DVS_MANAGER_H

#include <utility>
#include <mutex>
#include <set>
#include <map>
#include <list>
#include <string>

#include <config/Configuration.h>
#include <dal/Application.h>
#include <dal/TemplateApplication.h>
#include <dal/RunControlApplication.h>
#include <dal/Partition.h>
#include <dal/Segment.h>
#include <dal/Crate.h>
#include <dal/Computer.h>
#include <dal/OnlineSegment.h>
#include <dal/Binary.h>
#include <dal/HW_System.h>
#include <dal/HW_Object.h>
#include <TestManager/TestPolicy4Class.h>
#include <TestManager/TestPolicy4Object.h>
#include <dal/util.h>

//#include <TM/Client.h>
namespace daq::tm {
class Client ;
}

#include <dvs/exceptions.h>
#include <dvs/dvscomponent.h>

namespace daq::dvs {

class DVSManager {

public:

	enum Status {
		Ready, Bad, Busy
	};

	DVSManager(); // setReactor later
	virtual ~DVSManager();

	// loads configuration
	void loadDatabase(Configuration* configuration, const std::string& partition, const std::string& segment = "", bool recursive = true, bool skip_apps = false) ; // throw (CannotLoadDatabase )

	// returns LogRoot for initial partition
	const char * get_InitialLogRoot();

	// returns LogRoot for partition
	const char * get_PartitionLogRoot();

	//loads only testable components
	void LoadTestableOnly(const bool testable_only);

	static TestsSet getTestsForComponent(const Component* component, std::string scope, int level) ;

	// reset status of a component to Untested
	bool resetComponent(const std::string& object_id) ;

	// terminate a test by sending stop-test message to an object
	void stopTest(const std::string& object_id) ;

	// returns head of the tree of testable objects
	std::shared_ptr<Component> getConfiguration() const {
		return dvs_configuration_;
	}

	// Bad, Ready, Busy ? - client should check this prior to other calls
	Status status() {
		std::lock_guard<std::mutex> ml(busy_mutex_);
		return status_;
	}

	// try to set status to Busy or Ready
	// return false if Manager already Busy in attempt to set it Busy
	bool setBusy(bool flag);

	void setTestVerbosity(bool flag) ;

	static bool getTestVerbosity() {
		return verbosity_;
	}

	static const daq::core::Computer* getDefaultHost() {
		return default_host_;
	}

	static daq::tm::Client* getTestManager() {
		return tmClient_ ;
	}

	static Configuration* getConfDB() {
		return database_ ;
	}

	static Component*	findComponent(const std::string& id) ;

	// registers callback for database-change notification
	bool registerDatabaseCallback();

	// called from database change notification callback
	// virtual method: can be re-implemented in SetupManager
	virtual void reloadDatabase(bool skip_apps = false) ;
	// throw (CannotReloadDatabase );

	// void dropHardwareIfOFF(bool checked);

	// helper function to get policies for an object
	static void getPolicies4Object( std::list<const daq::tm::TestPolicy*>& policies, const DalObject* object) ;

protected:
	static const int DEFAULT_TEST_TIMEOUT = 45; 
	Component* dvs_partition_; // partition to set up
	std::shared_ptr<Component> dvs_configuration_; // root of DVS components tree

	static Configuration* database_;
	static daq::tm::Client* tmClient_;
	static const daq::core::Computer* default_host_;
	static const daq::core::Partition* partition_; // loaded partition

	bool use_config_; // load configuration from Configuration* or from datafile

	const char* database_filename_;
	std::string partition_name_ ;
	std::string segment_name_;
	const char* initial_log_root_;
	const char* partition_log_root_;

	const daq::core::Partition* initial_partition_; // initial partition
	const daq::core::OnlineSegment* online_segment_; // Online segment in the partition
	const daq::core::OnlineSegment* initial_infrastructutre_; //
//	const daq::core::Binary* pmg_binary_; // binary describing PMG_Agent
//	std::vector<std::string> def_tags_; // vector of default tags

	Status status_; // Busy, Bad, Ready
	std::mutex busy_mutex_; // mutex to protect access to status_

private:
	bool load_all_;
	bool recursive_;
	bool skip_apps_;
	// bool dropHwOFF_;	
	static bool verbosity_; // level of verbosity for TM

	static ComponentsMap all_components_ ;

	std::set<std::string>	loaded_stack ; // to protect from looping

	bool module_added;
	bool m_this_level_only; // flag to indicate whether only one test level should be considered
	const daq::core::Computer* getLocalHost() const;
	// daq::rm::RM_ClientExt* RMClient; // Resource Management Client
	daq::core::SubstituteVariables * substVar ;

	// map of class names to TestPolicies
	static std::map<std::string, const daq::tm::TestPolicy*> 	s_test_policies_class ;
	static std::map<std::string, const daq::tm::TestPolicy*> 	s_test_policies_object ;

	// hierarchically load objects tree
	bool loadSubtree(Component* parent, Component* group, std::vector<const DalObject*> objects, bool recursive = true) ;
	bool loadDalObject(Component* component, const DalObject* dalobject, bool recursive = true) ;
	bool loadSegment(const daq::core::Segment* segment, Component* parent, bool recursive = true) ;
	// throw (CannotLoadInstance )

	void dbCleanup();

	static void reloadDatabaseCallback(const std::vector<ConfigurationChange *> &, void *);

	//template<class T>
	//static TestsSet getTestsForComponent(const T* object, std::string scope, int level) ;

	// returns vector of TestableObjects following a relationship by string (e.g. Computer->Interfaces->vector<NIC>)
	std::vector<const DalObject*>
	getDependencies(const DalObject* object, const std::string& rel_name) ;

	// gets TOs for a parent TO/Component following TPs
	// returns list of pairs of TB and corresponding vector of child objects
	std::list<std::pair<const daq::tm::TestBehavior*, std::vector<const DalObject*>>>
	loadByTestPolicies(const DalObject*) ;

	// recursively navigates up in class hierarchy and finds all relevant policies for the class
	// stops at the first class with defined policy in each hierarchy branch
	// fills in the list of policies
	static void getPolicies4Class( std::list<const daq::tm::TestPolicy*>& policies, const std::string& class_name) ;

	// adds child physically if it does not exist, or by reference
	void addChild(Component* parent, Component* child, const daq::tm::TestBehavior* tbeh = nullptr);

};

// TODO missing in TM
inline void DVSManager::setTestVerbosity(bool flag) {
	if (flag) {
		verbosity_ = true;
	} else {
		verbosity_ = false;
	}
}

inline void DVSManager::LoadTestableOnly(const bool testable_only) {
	load_all_ = !testable_only;
}

inline const char* DVSManager::get_InitialLogRoot() {
	if (initial_partition_)
		return initial_partition_->get_LogRoot().c_str();
	else
		return "";
}

inline const char* DVSManager::get_PartitionLogRoot() {
	if (partition_)
		return partition_->get_LogRoot().c_str();
	else
		return "";
}

} //namespace daq::dvs

#endif // DVS_MANAGER_H
