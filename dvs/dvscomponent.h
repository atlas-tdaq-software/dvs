#ifndef DVS_COMPONENT_H
#define DVS_COMPONENT_H

#include <tuple>
#include <map>
#include <set>
#include <string>
#include <iostream>
#include <chrono>
#include <mutex>
#include <future>

#include <tbb/concurrent_unordered_map.h>
#include <tbb/concurrent_vector.h>

#include <config/Configuration.h>

#include <dal/TestableObject.h>
#include <dal/HW_System.h>
#include <dal/SW_Object.h>
#include <dal/Computer.h>
#include <dal/Module.h>
#include <dal/Crate.h>
#include <dal/Application.h>
#include <dal/Binary.h>
#include <dal/Variable.h>
#include <dal/Partition.h>
#include <config/DalObject.h>
#include <DFdal/HW_InputChannel.h>
#include <DFdal/ReadoutModule.h>

#include <TestManager/Test.h>
#include <TestManager/TestBehavior.h>
#include <TestManager/TestPolicy.h>

#include <TM/TestResult.h>
#include <TM/Handle.h>

#include <dvs/action.h>
#include <dvs/Reactor.h>

/*    enum    daq::tmgr::TestResult
{
        TmPass        = 0,   ///<test completed and confirmed the tested functionality
        TmUndef       = 182, ///<initial value
        TmFail        = 183, ///<test completed and confirmed that component is not working properly
        TmUnresolved  = 184, ///<test completed but it could not verify the functionality due to internal issues
        TmUntested    = 185, ///<test was not executed, e.g. some problems with test manager itself or with test configuration
        TmUnsupported = 186  ///<test was not executed (e.g. component is untestable) - in practice TmUntested is returned in this case
}

struct daq::tm::TestResult {
        uint32_t returnCode ; // return code of the last Executables
        std::string test_id ; // some meaningful single test name e.g. Test ID from DB
        std::string stdout ;  // combined std out/err from all Executables
        std::string stderr ;
        std::vector<const daq::tm::TestFailureAction*> actions ; // Actions
        std::string diagnosis ; // derived from Failures, if any
        std::chrono::microseconds duration ; // how long it took to execute the test
        std::chrono::system_clock::time_point test_time ;
        std::vector<std::shared_ptr<ers::Issue>> issues ;       // filled in case ComponentResult.result==Untested, i.e. there was an issue when launching a test
                                                                // also raised in TestHandle::getResult()
}

struct daq::tm::ComponentResult {
        tmgr::TestResult result; // enum { Passed, Undefined, Failed, Unresolved, Untested, Unsupported }
        std::vector<daq::tm::TestResult> test_results;    // detailed results of single Tests
        std::string component_id ; // classid@uid
}

class FailureAction
{
public:

const std::string command ;     // just copy from TestFailureAction
const std::string description ; // just copy from TestFailureAction
const std::string paramaters_json ;  // substited
}

*/ 

namespace daq {
namespace dvs {

/**
 * DVS Result, encapsulates global or cumulative testing result for a composite DVS Component and TM "local" test result for the component itself,
 * should it have any tests defined.
 */
struct Result {
	tmgr::TestResult globalResult ; /** subset of TM result: { TmUndef, TmPass, TmFail, TmUnresolved, TmUnsupported }, a global result of a composite component */ 
	daq::tm::ComponentResult componentResult ;   /** TestManager result structure, with a vector (possibly of 0 length if there are no tests) of and single test result (testResult.result) for the component */
  	std::string componentId ; // just object ID from configuration, in componentResult.component_id you have OKS classid@objid
	std::vector<FailureAction> actions ; // crafted JSON strings of { command: enum; parameters_json: string }

	Result(): globalResult(daq::tmgr::TmUndef) {} ;
} ;


/**
 * Scalar-result callback, called when test of a single component completed
 * To be used from DVS GUI
 * daq::tm::Result includes componentID which should link you to the element in the tree and in GUI
 * NB: status and messages from CLIPS (former DVSReactor) towards a Component are also updated internally and can be accessed when necessary,
 * when the test result is aquired
 */
using Callback = std::function<void(Result)> ;

/**
 * Vector-result callback, called when test of a component causes (aka policy) tests of other components,
 * It is called when all policy-tests completed for an Application, and contains all results for relevant components
 * like Computer, Binary and possibly other dependent Applications tested in the scope of the Application
 * std::string is operator-oriented string with description/diagnostics of the Result for the Application
 * (the same returned in future<> by Manager::testXXX)
 */
using CallbackV = std::function<void(std::vector<Result>)> ;

class Component ;
class DVSManager ;
class DVSApplication ;

using TestsSet = std::vector<const daq::tm::Test*> ;
using ComponentsMap = tbb::concurrent_unordered_map<std::string, Component*> ;
using ComponentsSet = tbb::concurrent_vector<Component*> ;
using ComponentRef = Component* ;

std::ostream& operator<< ( std::ostream& , const Component* );

class Component: public Reactor
{
friend class DVSManager ;
friend class DVSApplication ;

public:

	enum Type { Configuration = 0, Ipc, Mrs, RunControl, Pmg, Programs, Network, 
                   Hardware, Detector, Crate, Module, Computer, 
                   IpcGeneralServer, IpcServer, IsServer, RunController, 
                   MrsServer, PmgAgent, Program , RdbServer, RmServer,
                   VmeInterface, Services, PmgSupervisor, Is, Rdb, Monitoring,
                   CompositeEntity, Link, Application, Links, CorbaServer, 
                   InputChannel, Interface } ;

	/**
	 * Command which can be processed for the Component
	 */
	enum class Command { None, Test, TestById, Reset, Stop } ;

	/**
	 * extension of tmgr::TestResult
	 */
	enum Status {
		PASSED = 		tmgr::TmPass, // test completed and Passed
		UNDEFINED = 	tmgr::TmUndef, // initial value, result not yet received
		FAILED = 		tmgr::TmFail,  // test completed and Failed
		UNRESOLVED = 	tmgr::TmUnresolved, // test completed but it could not verify the functionality due to internal issues
		UNTESTED = 		tmgr::TmUntested, // failed to launch test (==UNRESOLVED to client)
		UNSUPPORTED = 	tmgr::TmUnsupported, // no tests defined (==UNTESTED)
		INPROGRESS = 	187 //lest launched, waiting for result
	} ;

	/**
	 * order of container testing
	 */
	enum TestOrder { First, Last } ; 

	virtual ~Component() ;
	
	// object name, "Name" attribute from the configuration
	const std::string &	name()						const ;

	// object unique ID, composed in the constructor
	const std::string&	key()						const ;
    Type				type()						const ;

	const ComponentsSet&	children()				const ;
	const ComponentsSet&	dependencies()			const ;
	const ComponentsSet&	follow_ups()			const ;

	const daq::core::TestableObject*	testableObject()	const { return testable_object_ ; } ;
	const DalObject*					dalObject()			const { return dal_object_ ; } ;

	bool isTestable()	const { return is_testable_ ; } ;

	const std::string&		getHelp()				const ;
	const std::string&		getClassName()			const ;
	const std::string&		getOksUid()				const ;

	bool					SyncPolicy()			const ;
	bool					StopOnError()			const ;
	TestOrder				ParentTestOrder()		const ;
		
	// TM callback std::function<void(const daq::tm::Result&)>
	void testCompleted( const daq::tm::ComponentResult& result ) ;

	// to be called from manager::testXXX methods:
	// at the start of testing, registers user callback and resets the promise, returning future of the Result
	// upon completion the tests, in CLIPS status() callback, fulfill the promise
	// bool is true when the future is already promised and was reset (i.e. another testing session is ongoing)
	std::tuple<std::future<Result>, bool> setCallback( Callback cb ) ;

	// same for vector callback
	std::tuple<std::future<Result>, bool> setCallback( CallbackV cb ) ;

	// DVS results of the testing
	const Result& getResult() { return m_result ; } 

	// results of the testing
	const daq::tm::ComponentResult& getComponentResult() { return m_result.componentResult ; } 

	// result of the testing, single value, part of ComponentResult
	tmgr::TestResult getTestResult() { return m_result.componentResult.result ; } 

	// get test results from all subtree (used in RC API)
	void getTestResults(std::vector<Result>& results) ; 

	const std::string& getDiagnosis() 		{ std::lock_guard<std::mutex> ml(m_update_mutex); return m_diagnosis ; } 
	
	// non-copyable
	Component(const Component&) = delete ;
	Component& operator=(const Component&) = delete ;

	// reset TM result, recursively
	// if test_result_only=true, only flush TM result, not CLIPS test status: to be used from RC 
	void resetComponent(bool test_result_only = false) ;

	// to be called from the Engine
	void startTest() ;

	// uses stored TMHandle to interrupt the test
	void stopTest() ;

	// set m_status and m_result.globalResult of a composite component, defined/called by the Rules
	void setStatus(Status status) ;
	Status getStatus() const ;
	void setCommand(Command command) ;
	Command getCommand() const ;
	void setTestById(const std::string&) ;

	// set test result of individual test for this component (m_result.componentResult), called from TM result callback
	void setTestResult(daq::tmgr::TestResult result) ;

	std::weak_ptr<daq::tm::TestHandle> getTestHandle() const { return test_handle_ ; } ;

	void get_dependencies( std::vector<ReactorRef>&) const ;

	int getTestLevel() const ;
	std::string getTestScope() const ;
	void setTestLevel(int scope) ;
	void setTestScope(const std::string& scope) ;

/**
 * Used in testApplication, where RC passes a vector of runtime backup hosts, which 
 * result of testing is to be ignored
 */
	void setBackupHosts(const std::set<std::string>& bhosts)  ;

	bool isBackupHost(const std::string& host) const ;

protected:
	// constructor
	// oks_oid = 0 when there is no this object in configuration
	// private, available for friend DVSManager and subclasses only
	Component(	const std::string& id, Type, const DalObject* = nullptr ) ;

	// constructor for TB containers, where display name need to be cooked a bit
	Component( const std::string& name, const daq::tm::TestBehavior& beh ) ;

	std::string					name_ ;		// typically the Name attribute in DB
	std::string					help_link_ ;

	const daq::core::TestableObject* 		testable_object_ ;		// ref to database object, needed in TM->test
	std::weak_ptr<daq::tm::TestHandle>		test_handle_ ;			// non-owning ref to handle, used to stop the tests
	TestsSet								tests_ ;				// tests defined for the object at the moment of testing
	Result									m_result ;  		// combined result
	Command									m_command ;  		// current command
	Status									m_status ;			// extended resuly
	std::string								m_test_by_id ;  		// if test by ID requested
	std::vector<Result>						m_results ;  		// vector of test results for RC tests
	std::chrono::system_clock::time_point	test_start_time_ ;		// time point when the test was launched
	std::promise<Result> 					m_promised_result ;		// promised testing completion and result
	bool 									m_is_promised ;		// promise is already initialied,i testing is ongoing
	static Callback							s_callback ; 			// scalar callback, static, shared by multiple Components
	CallbackV								m_callback_v ; 			// vector callback

private:
	std::string		key_ ;			// unique ID in the tree, made in the constructor
	Type			type_ ;         // helpful type for display purposes
	std::string		class_name_ ;	// OKS class name (BaseApplication in case of Templated apps)
	std::string		obj_uid_ ;		// OKS UID
	ComponentsSet	children_ ;		// set of direct children components
	ComponentsSet	deps_ ;			// set of indirect children dependencies
	ComponentsSet	follow_ups_ ;	// additional components to test following test FailureActions
	std::set<std::string> backup_hosts_ ; // passed from Applicaiton to host() and backup_hosts() group
	const daq::tm::TestBehavior* test_beh_ ; // TestBehavior 
	bool 			is_testable_ ;

	bool									sync_policy_ ;   // sync/async testing of children
	bool 									stop_on_error_ ; // to stop at first child test failure
	TestOrder								parent_test_order_ ; // first or last order of parent testing

	std::string	m_diagnosis ;	// human-readable diagnosis, resume of the testing of the component
	int m_test_level ; // current test level
	std::string m_test_scope ; // current test scope

	// protection mutex for different fields
	mutable std::mutex m_update_mutex ;

	const DalObject* dal_object_ ;		// ref to original DalObject object, needed in DVS GUI

	// interface to TM
	// async test launching, TMHandle stored
	void testComponent(const std::string& scope, int level, const std::string& test) ;

	// dynamically added dependency, a follow-up test component. Need to be emptied at next TestComponent or Reset
	void addTestFollowUp( Component* child ) ;

	// dynamically added dependency, a follow-up 
	void clearTestFollowUp( ) ;

	// composite relationship, direct children, needed for construction/destruction
	bool addChild( Component* child, const daq::tm::TestBehavior* = nullptr ) ;

	// week dependency, also includes all composite children, needed for logic
	void addDeps( Component* dep ) ;
};


class DVSApplication: public Component{

 public:

	DVSApplication( const daq::core::BaseApplication* app ) ;

	const std::string& host() const { return host_; } ;
	const daq::core::BaseApplication* appConfig() const { return app_cfg_ ; } ;

private:

	std::string host_ ;
	const daq::core::BaseApplication* app_cfg_ ;
}; 

	//
	// Inline methods implementations
	//
	
inline
const std::string & Component::getHelp() const
{
	return help_link_;
}

inline
const std::string & Component::name() const
{
	return name_;
}	

inline
const std::string & Component::key() const
{
	return key_;
}	

inline
Component::Type Component::type() const
{
    return type_;
}

inline
const ComponentsSet& Component::children() const
{
	return children_;
}

inline
const ComponentsSet& Component::dependencies() const
{
	return deps_;
}

inline
const ComponentsSet& Component::follow_ups() const
{
	std::lock_guard<std::mutex> ml(m_update_mutex);
	return follow_ups_;
}

inline	
void Component::addTestFollowUp( Component* child ) 
{
	{ std::lock_guard<std::mutex> ml(m_update_mutex);
	follow_ups_.push_back(child) ; }
	send_event() ; // activate corresponding rules that start additional tests
}

inline void Component::clearTestFollowUp( ) 
{
	follow_ups_.clear() ;
}

inline
const std::string& Component::getClassName() const
{
	return class_name_ ;
}

inline
const std::string& Component::getOksUid() const
{
	return obj_uid_ ;
}

inline
bool Component::SyncPolicy() const
{
	return sync_policy_ ;
}

inline
bool Component::StopOnError() const
{
	return stop_on_error_ ;
}

inline
Component::TestOrder Component::ParentTestOrder() const
{
	return parent_test_order_ ;
}

inline
Component::Status Component::getStatus() const
{
	std::lock_guard<std::mutex> ml(m_update_mutex) ;
	return m_status ;
}

inline
void Component::setCommand(Command cmd) 
{ 
	{ std::lock_guard<std::mutex> ml(m_update_mutex) ;
	m_command = cmd ; }
	send_event() ; // trigger rules
}

inline
Component::Command Component::getCommand() const
{
	std::lock_guard<std::mutex> ml(m_update_mutex) ;
	return m_command ;
}

inline
void Component::setTestResult(daq::tmgr::TestResult result) 
{ 
	{ std::lock_guard<std::mutex> ml(m_update_mutex);
	m_result.componentResult.result = result ; }
	send_event() ; // trigger rules
}

inline
void Component::setTestById(const std::string& test) 
{
	m_test_by_id = test ;
}

inline
bool Component::isBackupHost(const std::string& host) const
{
	std::lock_guard<std::mutex> ml(m_update_mutex);
	return backup_hosts_.find(host) != backup_hosts_.end() ;
}

}}  //namespace daq::dvs

#endif // DVS_COMPONENT_H
