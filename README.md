# Package DVS - Diagnostics and Verification System (framework)

A layer above [Test Manager](https://gitlab.cern.ch/atlas-tdaq-software/TM), organizing test sequences and follow-ups according to TestBehaviour defined in configuration. Used by [DVS GUI](https://gitlab.cern.ch/atlas-tdaq-software/dvs_gui) to allow a user (expert) to test an arbitrary subset of a TDAQ configuration in a handy GUI and by Run Control for testing individual applications and their dependencies in runtime (including continueous testing in Running state). Tests are defined according to Test Repository schema in TM.

See [Twiki](https://twiki.cern.ch/twiki/bin/view/Atlas/DaqHltTestManager) documentation for more details about tests configuration.

