//
// FILE: testdal_dump.cpp
//
// Dumps all test-related information about a configuration,  i.e. prints out all testable objects 
// along with tests defined for them.
// Uses SW & DF DALs to read the configuration and Test DAL to retreive test information.
// 
// Implementation:
//	Andrei Kazarov - Oct 2001
// Modified:
//	AK July 2003 - new DAL

#include <cmdl/cmdargs.h>

#include <tmgr/testdal.h>

#include <dvs/dvscomponent.h>
#include <dvs/slink.h>

void dump_tests( const std::list<ConfdbTest>& ) ;

int main(int argc, const char* argv[])

{
// get database file on input

CmdArgStr	data_file('d', "data", "data-file-name", "Datafile with configuration.", CmdArg::isREQ) ;
CmdArgStr	oid('p', "oid", "object-id", "OKS Object ID in form ClassName@ObjectID. If skipped, all objects are dumped.") ;
CmdLine		cmd(*argv, &data_file, &oid, NULL) ;
cmd.description("Prints out all testable objects from the configuration along with associated tests.") ;
CmdArgvIter  	arg_iter(--argc, ++argv) ;

cmd.parse(arg_iter);

bool dump_all = true ; 			// dump all objects
std::string class_name, object_id ;

if ( !oid.isNULL() )			// dump specific object with given oid
	{
	dump_all = false ;	
	std::string tmp = (const char*)oid ;
	class_name = tmp.substr(0,tmp.find('@')) ;
	object_id = tmp.substr(tmp.find('@')+1) ;
	}

// keep more messages out
putenv(const_cast<char*>("OKS_KERNEL_SILENCE=1")) ;
putenv(const_cast<char*>("CONFDB_MESSAGES=ERRORS")) ;

// load federated database as Confdb object
// uses TDAQ_DB_SCHEMA from the process environment
OksConfiguration kernel ;
Configuration confdb( data_file, &kernel ) ;

// create TestConfiguration, an entry point for all TestDAL functionality
ConfdbTestConfiguration testconf(confdb) ;

ConfdbTestConfiguration::Status status = testconf.getStatus() ;

if ( status != ConfdbTestConfiguration::Success )
	{
	std::cout << "Failed to load Test Configuration: " << status << std::endl ;
	exit(-1) ;
	}

// load SW Configuration from the same database
ConfdbConfiguration conf(&confdb) ;

ConfdbConfiguration::Status status1 = conf.get_status() ;

if ( status1 != ConfdbConfiguration::Success )
	{
	std::cout << "Failed to load SW Configuration: " << status1 ;
	exit(-1) ;
	}
	
const std::list<const ConfdbTest*>& testlist = testconf.getAllTests() ;

if ( dump_all )
{
std::cout << "**********************************************************************" << std::endl ;	
std::cout << "<<<<<< All tests defined in the configuration:\n" << std::endl ;

std::list<const ConfdbTest*>::const_iterator t_it = testlist.begin() ;
for ( ; t_it != testlist.end() ; t_it++ ) (*t_it)->print(0, std::cout) ;

std::cout << "**********************************************************************" << std::endl ;	
std::cout << "<<<<<< All testable computers in the configuration:\n" << std::endl ;
}

const std::list<ConfdbComputer*>& comps = conf.computers() ;
std::list<ConfdbComputer*>::const_iterator c_it = comps.begin() ;
for ( ; c_it != comps.end() ; c_it++ )
	{
	if ( !dump_all && ((*c_it)->get_id() != object_id || (*c_it)->get_class_name() != class_name) )
		continue ;
	std::list<ConfdbTest> *tests_for_comp = testconf.getTest4Object(*(*c_it)) ;
	if ( !tests_for_comp )
		std::cout << "No tests found for the computer " << (*c_it)->get_id() << std::endl ;
	else
		{
		std::cout << "Test(s) for Computer " << (*c_it)->get_id() << ":" << std::endl ;
		dump_tests(*tests_for_comp) ;
		}
	}

const std::list<ConfdbCrate*>& crates = conf.crates() ;
std::list<ConfdbCrate*>::const_iterator cr_it = crates.begin() ;

if ( dump_all ) {
std::cout << "**********************************************************************" << std::endl ;	
std::cout << "<<<<<< All testable modules in the configuration:\n" << std::endl ; }

for ( ; cr_it != crates.end() ; cr_it++ )
	{
	std::list<ConfdbModule *>::const_iterator nextm = (*cr_it)->modules().begin() ;
	for ( ; nextm != (*cr_it)->modules().end() ; nextm++ )
		{
		if ( !dump_all && ((*cr_it)->get_id() != object_id || (*cr_it)->get_class_name() != class_name) )
			continue ;
		std::list<ConfdbTest> *tests = testconf.getTest4Object(*(*nextm)) ;
		if ( !tests )
			std::cout << "No tests found for the module " << (*nextm)->get_id() << std::endl ;
		else
			{
			std::cout << "Test(s) for Module " << (*nextm)->get_id() << ":" << std::endl ;
			dump_tests(*tests) ;
			}
		}
	}

if ( dump_all ) {
std::cout << "**********************************************************************" << std::endl ;	
std::cout << "<<<<<< All testable S-Links in the configuration:\n" << std::endl ; }

OksObject *partition_obj = confdb.get_root_object() ;
if ( !partition_obj ) exit(-1) ;

OksKernel* okskernel = partition_obj->GetClass()->GetOksKernel() ;
if ( !okskernel )   exit (-1) ;
	
OksClass* slink_class = okskernel->FindClass( SLink_classname ) ;
if ( !slink_class ) exit(-1) ;

std::list<OksObject *> * slinks = slink_class->create_list_of_all_objects() ;
if ( slinks )
	{
	std::list<OksObject *>::const_iterator sl_it = slinks->begin() ;

	for ( ; sl_it != slinks->end() ; ++sl_it )
		{
		ConfdbSLink slink(*(*sl_it)) ;
		if ( !dump_all && (slink.get_id() != object_id || slink.get_class_name() != class_name) )
				continue ;
		std::list<ConfdbTest> *tests = testconf.getTest4Object(slink) ;
		if ( !tests )
			std::cout << "No tests are found for S_Link " << slink.get_id() << std::endl ;
		else
			{
			std::cout << "Test(s) for S_Link " << slink.get_id() << ":" << std::endl ;
			dump_tests(*tests) ;
			}	
		}
	}
else	
	std::cout << "No S-Links found in the configuration. " << std::endl ;
	
	
if ( dump_all ) {
std::cout << "**********************************************************************" << std::endl ;	
std::cout << "<<<<<< All testable Applications in the configuration:\n" << std::endl ; }

// retreive list of all applications from this partition
std::list<ConfdbApplication *>::const_iterator apps = conf.applications().begin() ;

for ( ; apps != conf.applications().end(); apps++ )
	{
	if ( !dump_all && ((*apps)->get_id() != object_id || (*apps)->get_class_name() != class_name) )
			continue ;
	std::list<ConfdbTest>* tests = testconf.getTest4Object(*(*apps)) ;
	if ( !tests )
		std::cout << "No tests found for Application " << (*apps)->get_id() << std::endl ;
	else
		{
		std::cout << "Test(s) for Application " << (*apps)->get_id() << ":" << std::endl ;
		dump_tests(*tests) ;
		}
	}	
}

void dump_tests(const std::list<ConfdbTest>& tl)
{
std::list<ConfdbTest>::const_iterator t_it = tl.begin() ;
for ( ; t_it != tl.end() ; t_it++ )
	(*t_it).print(0, std::cout) ;
}

