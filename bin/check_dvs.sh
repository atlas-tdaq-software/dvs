#!/bin/sh

partition="onlsw_test"
database=$partition.data.xml

pm_part_hlt.py -p $partition || { echo "Failed to generate partition"; exit 1; }

echo "**************************************************************************"
date
echo "DVS check starting: "

export TDAQ_IPC_INIT_REF=file:/`pwd`/ipc.ref
echo "TDAQ_IPC_INIT_REF=$TDAQ_IPC_INIT_REF"
#echo "Initilizing DVS: Starting initial partition"
#setup_daq $database initial -ng

ipc_server &
pidipcg=$!

function cleanup() { echo "Stopping global IPC server" ; kill $pidipcg ; sleep 1; kill -9 $pidipcg > /dev/null 2>&1 ; }

trap cleanup EXIT SKILL SIGTERM

count=0;
while test $count -lt 5
do
        sleep 2
        test_ipc_server > /dev/null 2>&1 && { echo " OK!" ; break ; } 
        count=`expr $count + 1`
done

dvs_init $database $partition || { echo "DVS check ERROR: dvs_init failed to start initial services!" ; exit 1 ; }

echo "Starting dvs_console: "
{ dvs_console -d $database -p $partition <<MYEND
tree
test Segment_setup
testV Setup 
testApp DF
reload 
reset Setup
test Setup
quit
MYEND
} || { echo "DVS check ERROR: dvs_console failed"; ipc_rm -f -i ".*" -n ".*" ; exit 2 ; }

echo
echo "Cleaning everything with ipc_rm: "
ipc_rm -i ".*" -n ".*"
rm -f $database
sleep 1
cleanup 

echo "DVS check PASSED"
date
echo "=========================================================================="
