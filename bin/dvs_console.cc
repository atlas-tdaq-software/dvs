#include <ctime>
#include <csignal>
#include <cstdlib>

#include <map>
#include <set>
#include <string>
#include <mutex>
#include <iomanip>

#include <boost/tokenizer.hpp>

#include <cmdl/cmdargs.h>
#include <ipc/core.h>
#include <config/Configuration.h>
#include <TestManager/ExecutableTest.h>
#include <dvs/dvscomponent.h>
#include <dvs/manager.h>
#include <dvs/exceptions.h>

using namespace boost;


// using namespace daq::dvs ;

// to report errors from application
  ERS_DECLARE_ISSUE(
    dvs,
    Error,
    message,
    ((std::string)message)
  )

CmdArgBool	verbose('v', "verbose", "turn on 'verbose' output") ;

#undef  DBG
#ifdef  __DEBUG__
        #include <owl/time.h>
        #define DBG(x)  { std::cerr << "###DVS [" << OWLTime().c_str() << "] " << x << std::endl ; } ;
#else
        #define DBG(x)  {;} ;
#endif

using std::setw;

static void printUsage()
{
	std::cout    << "  exit (quit)" << std::setw(20) << " - exit DVS" << std::endl
		<< "  help" << std::setw(20) << " - print this information" << std::endl 
		<< "  reload " << std::setw(20) << " - reload database (usually done automatically)" << std::endl
		<< "  reset <component name>" << std::setw(20) << " - set the component to initial state" << std::endl
		<< "  level  <level>" << std::setw(20) << " - set testing level" << std::endl
		<< "  scope  <scope>" << std::setw(20) << " - set testing scope selector (default=any)" << std::endl
		<< "  test  <component name>" << std::setw(20) << " - verify the component" << std::endl
		<< "  test  <component name> <test ID>" << std::setw(20) << " - run a particular test for the component" << std::endl
		<< "  testV  <component name>+ " << std::setw(20) << " - verify a list of components using RC API" << std::endl
		<< "  testApp  <component name> " << std::setw(20) << " - verify an Applicaiton with backup hosts using RC API" << std::endl
		<< setw(8) << "  ^C interrupts the current testing session" << std::endl
		<< setw(8) << "  ^D exits the application" << std::endl
		<< "  list <component name>" << std::setw(40) << " - list available tests and actions for a component" << std::endl
		<< "  verbose\t\t\t- see detailed output from tests" << std::endl
		<< "  silent\t\t\t- modest output" << std::endl
		<< "  tree\t\t\t- print out tree of all components" << std::endl ;
}

// std::function<void(daq::tm::Result)> Callback ;

std::mutex out_mtx ;

void MyCallback(daq::dvs::Result result)
{
std::lock_guard lock (out_mtx) ;

ERS_DEBUG(1, "DVS callback received for " << result.componentId) ;

// ignore reset
if ( result.globalResult == daq::tmgr::TestResult::TmUndef ) return ;

std::cerr << "%%%%%%%%% Test of " << result.componentId << " completed: GR:" << result.globalResult << ", TR: " << result.componentResult.result<< std::endl ;
if ( result.componentResult.test_results.empty() ) return ;

if ( result.componentResult.result == daq::tmgr::TestResult::TmUntested )
	{
	std::cerr << "Failed to launch tests. Issues: " << std::endl ;
	for ( auto tr: result.componentResult.test_results )
		{
		for ( auto issue: tr.issues )
			{ ers::error (*issue) ; } 
		}
	return ;
	}

std::cerr << "%%%%%% Test results:" << std::endl ;
	for ( const auto& res: result.componentResult.test_results )
		{
		std::stringstream time ;
		std::time_t tt = system_clock::to_time_t(res.test_time) ;
		struct std::tm * ptm = std::localtime(&tt);
		time << std::put_time(ptm, "[%T] ") ;

		std::cerr << "\t" << time.str() << res.test_id << " test result: " << res.return_code << std::endl ;
		if ( !res.diagnosis.empty() )std::cerr << "%%%%%% Diagnosis: " << res.diagnosis << std::endl ;

		if ( verbose )
			{
			std::cerr << "\t## STD OUT\n" << res.stdout << std::endl ;
			std::cerr << "\t## STD ERR\n" << res.stderr << std::endl ;
			}
				
		if ( res.actions.size() )
			{
			std::cerr << "%%%%%% Test Failure Actions:" << std::endl ;
			for ( auto action: res.actions )
				{
				std::cerr << "\t" << action << std::endl ;
				}
			}
		}

std::cerr << "%%%%%% Actions:" << std::endl ;
for ( const auto act:  result.actions )
	{
		std::cerr << "\t" << act.command << std::endl ;
		std::cerr << "\t" << act.parameters_json << std::endl ;
	}
	
}

void MyCallbackV(std::vector<daq::dvs::Result> results)
{
if ( results.empty() )
	{
	ERS_LOG("UNEXPECTED: zero-size result is passed in CallbackV") ;
	return ;
	}

ERS_DEBUG(2, "DVS callbackV received for " << results[0].componentId) ;

	std::cerr << "%%%%%% Vector Test of " << results[0].componentId << " completed: " << results[0].globalResult 
	<< " including " << results.size() - 1 << " dependent components: " << std::endl ;
	for ( auto res: results )
		{
		std::cerr << "\t" << res.componentId << " tested: " << res.componentResult.result << std::endl ;
		for ( auto tres: res.componentResult.test_results )
			{
			std::cerr << "\t\t" << tres.test_id << " result: " << tres.return_code << std::endl ;
			std::cerr << "\t\tDiagnosis: " << tres.diagnosis << std::endl ;
			if ( verbose )
				{
				std::cerr << "\t\t## STD OUT #########" << tres.stdout << std::endl ;
				std::cerr << "\t\t## STD ERR #########" << tres.stderr << std::endl ;
				}

			for ( auto action: tres.actions )
				{
				std::cerr << "\t\tACTION: " << action << std::endl ;
				}
			}		
		}
}

namespace {
daq::dvs::Component* tested = nullptr ;
daq::dvs::Manager* dvsm = nullptr ;
}

void
inter_handler( int )
{
if ( tested != nullptr && dvsm != nullptr)
	{
	std::cerr << "##### Interrupted: stopping testing of " << tested->key() << std::endl ;
	dvsm->stopTest(tested) ;
	}
else
	{
	std::cerr << "##### Izvinite, ^C is reserved for stopping the tests. To exit dvs_console, use ^D." << std::endl ;
	}
}

int main ( int argc, char **  argv)
{
	try	{
		IPCCore::init( argc, argv ) ;
	//	daq::pmg::Singleton::init() ;
		}
  	catch 	( daq::ipc::Exception & ex )
		{
		ers::error( ex ) ;
		exit(1) ;
		}

    	signal( SIGINT , inter_handler );

      // Declare arguments
      
	CmdArgStr	data_file('d', "data", "data-file-name", "Configuration to test.", CmdArg::isREQ);
	CmdArgStr	partition('p', "partition", "partition", "selected partition", CmdArg::isREQ);
	CmdArgStr	segment('s', "segment", "segment", "selected segment (all by default)") ;
	CmdArgBool	recursive('r', "recursive", "load subsegments of the segment (default: no)");
	CmdArgBool	skip_apps('w', "hw-mode", "do not load applicaitons (default: no)");
	CmdArgBool	check_mode('c', "check-mode", "exit at any error, to be used in unit tests");
 
       // Declare command object and its argument-iterator
	CmdLine  cmd(*argv, &data_file, &partition, &segment, &recursive, &skip_apps, &check_mode, &verbose, NULL);
	CmdArgvIter  arg_iter(--argc, ++argv);
  
       // Parse arguments
	cmd.parse(arg_iter);

	bool recurs = recursive.flags() && CmdArg::GIVEN ? recursive : false ;
	bool skipapp = skip_apps.flags() && CmdArg::GIVEN ? skip_apps : false ;
	bool checkmode = check_mode.flags() && CmdArg::GIVEN ? check_mode : false ;

	std::string segment_name  ;

	if ( segment.flags() & CmdArg::GIVEN )
		{ segment_name = segment ; }

	// keep more messages out
	putenv(const_cast<char*>("OKS_KERNEL_SILENCE=1")) ;
	putenv(const_cast<char*>("CONFDB_MESSAGES=ERRORS")) ;

	std::string env = "TDAQ_DB_DATA=" ;
	env.append(data_file) ;
	putenv(const_cast<char*>(env.c_str())) ;

	try {
		dvsm	= new daq::dvs::Manager() ;
	}
	catch ( ers::Issue& ex ) {
		ers::fatal ( ex ) ;
		exit(1) ;
	}
		
	// MyReactor	reactor(verbose) ;
	int test_level = 255 ;
	std::string test_scope = "any" ;

	std::shared_ptr<Configuration> confdb ;	
	try 	{ confdb = std::make_shared<Configuration>( std::string("oksconfig:")+std::string(data_file) ) ; }
	catch 	( daq::config::Exception & ex)
		{
		std::ostringstream text ;
		text << "Failed to load configuration database "<< data_file ;
		ers::error ( dvs::Error(ERS_HERE, text.str(), ex) ) ;
		}

	std::shared_ptr<daq::dvs::Component>    tree;
	try { tree = dvsm->loadConfiguration(*confdb, std::string(partition), segment_name, recurs, skipapp) ; }
	catch ( daq::dvs::CannotLoadDatabase & ex )
		{
		ers::error(ex) ;
		exit(1) ; 
		} ;	
		
	std::cout << std::endl << "Type 'help' for list of commands" << std::endl;
	
	while ( std::cin ){
				
		std::cout << "dvs> ";
		
        	std::string in;
        	
        	std::cin >> in;
		if ( in == "exit" || in == "quit" )
			break;
			
		if ( in == "reload")
		{
			try {
				tree = dvsm->reloadConfiguration(skipapp) ;
				}
			catch ( ers::Issue& ex )
				{
				ers::error(ex);
				if ( checkmode ) { exit(1) ; } 
				}
			continue;
		}

		if ( in == "list")
		{
			std::cin >> in;
			daq::dvs::TestsSet tests_for_obj ;
			
			try {
				daq::dvs::Component * comp = dvsm->findComponent(in) ;
				if ( comp == nullptr )
					{
					std::cerr << "ERROR: can not find object " << in << " in the tree" << std::endl ;
					if ( checkmode ) { exit(1) ; } 
					continue ;
					}
				tests_for_obj = dvsm->getTestsForComponent(comp);
			}
			catch   ( ers::Issue & ex ) {
			    	std::cerr << "ERROR: can not get tests for object " << in << ": "
				<< ex.what() << std::endl ;
				if ( checkmode ) { exit(1) ; } 
				continue ;
			}

			for ( auto test : tests_for_obj )
				{
			    	std::cout << test->UID() << std::endl ;
					const daq::tm::ExecutableTest* etest = confdb->cast<daq::tm::ExecutableTest>(test) ;
			    	if ( etest )  { etest->print(2,false,std::cout) ; }
					else { std::cerr << "Failed to cast to ExecutableTest"; }
				}

			continue;
		}
		if ( in == "reset")
		{
			std::cin >> in ;
			daq::dvs::Component * comp = dvsm->findComponent(in) ;
			if ( comp != nullptr )
				{ dvsm->resetComponent(comp); }

			sleep(1) ;
			std::cerr << "#############################################################\n" ;

			continue;
		}
		if ( in == "level") {
			std::cin >> test_level;
			dvsm->setTestScopeLevel(test_scope, test_level);
			std::cout << "Current testing level = " << test_level << std::endl;
			continue;	
		}
		if ( in == "scope") {
			std::cin >> test_scope;
			dvsm->setTestScopeLevel(test_scope, test_level);
			std::cout << "Testing scope = " << test_scope << std::endl;
			continue;	
		}
		if ( in == "test" )
		{
			std::string input, test, comp;
			std::getline(std::cin, input) ;

			std::cerr << "#############################################################\n" ;

			size_t delim = input.find(" ", input.find_first_not_of(" ")) ;
			if ( delim != std::string::npos )
				{
				comp = input.substr(input.find_first_not_of(" "), delim-1) ;
				test = input.substr(input.find_first_not_of((" "), delim), input.length());
				}
			else
				{
				comp = input.substr(input.find_first_not_of(" ")) ;
				}

			tested = dvsm->findComponent(comp) ; 

			if ( tested != nullptr ) {
				daq::dvs::Result res ;				
				if ( test.empty() )
					{
		std::cerr << "Async testing of component " << tested << " \n" ;
					std::future<daq::dvs::Result> futres = dvsm->testComponent(tested, (daq::dvs::Callback)&MyCallback, test_scope, test_level) ;
					res = futres.get() ; // wait for testing completed
					}
				else
					{
		std::cerr << "Sync testing by test id: " << test << " \n" ;
					res = dvsm->testComponent(tested, test, test_scope, test_level) ;
									std::cout << "Diagnostics:\n" << tested->getDiagnosis() << std::endl ; 
					
					if ( verbose ) {
						for ( auto tr: tested->getComponentResult().test_results )
						{
						std::cout << "#####\n\tTest " << tr.test_id << ": return code: " << tr.return_code << "\n" ;
						std::cout  << "\tStdOut:\n\t" << tr.stdout << "\n" ;
						std::cout  << "\tStdErr:\n\t" << tr.stderr << "\n" ;
						}
					}
				std::cout << "#############################################################\n\
Testing of " << comp << " completed: " << res.globalResult << std::endl ;
				} 
			} else	{
				std::cerr << "ERROR: component " <<  comp << " not found in the tree\n" ;
				if ( checkmode ) { exit(1) ; } 
			}

			tested = nullptr ;

			std::cerr << "\n#############################################################\n" ;
			continue;
		}
		if ( in == "testV" )
		{
			std::vector<std::future<daq::dvs::Result>> tobetested ;
			std::string input ;

			std::getline(std::cin, input) ;
			boost::char_separator<char> sep(", ") ;
			boost::tokenizer<char_separator<char>> tokens(input, sep) ;

			for ( const auto& token: tokens )
				{
	  			daq::dvs::Component* tested = dvsm->findComponent(token) ;
				if ( tested != nullptr )
					{
					// std::future<daq::dvs::GlobalResult> futres = dvsm->testComponent(tested, (daq::dvs::CallbackV)&MyCallbackV, test_scope, test_level) ;
					tobetested.push_back( dvsm->testComponent(tested, (daq::dvs::CallbackV)&MyCallbackV, test_scope, test_level) ) ;
					}
				else
					{
					std::cerr << "ERROR: component " << token << " not found in the tree\n" ;
					if ( checkmode ) { exit(1) ; } 
					break ;	
					}
				}
			
			for ( auto fres = tobetested.begin();  fres != tobetested.end(); fres++ ) { daq::dvs::Result res = (*fres).get() ; } ;

			std::cout << "#############################################################\n\
Testing of " << tobetested.size() << " components completed! " << std::endl ;

			std::cerr << "\n#############################################################\n" ;
			continue;
		}

		if ( in == "testApp" )
		{	
			std::cerr << "\n#############################################################\n" ;
			std::string input ;
			std::vector<std::future<daq::dvs::Result>> results ;
			std::getline(std::cin, input) ;

			boost::char_separator<char> sep(", ") ;
			boost::tokenizer<char_separator<char>> tokens(input, sep) ;

			for ( const auto& appname: tokens )
			  {	
			  daq::dvs::Component* app = dvsm->findComponent(appname) ; 

			  if ( app != nullptr )
				{
				daq::dvs::Result res ;
				const daq::core::BaseApplication* baseapp = confdb->cast<daq::core::BaseApplication>(app->testableObject());
				if ( baseapp )
					{
					std::cerr << "Async testing of Application " << appname << " \n" ;
					std::set<std::string> backup_hosts ;
					for ( const auto& bh: baseapp->get_backup_hosts() ) backup_hosts.emplace(bh->UID()) ;
					results.push_back(dvsm->testApplication(app, backup_hosts, (daq::dvs::CallbackV)&MyCallbackV, test_scope, test_level)) ;
					}
				else
					{
					std::cerr << "ERROR: component " << appname << " is not a BaseApplication\n" ;
					if ( checkmode ) { exit(1) ; } 
					}
				}
			  else
				{
				std::cerr << "ERROR: component " << appname << " not found in the tree\n" ;
				if ( checkmode ) { exit(1) ; } 
				}
			  }

			for ( auto fres = results.begin();  fres != results.end(); fres++ )
				{
				daq::dvs::Result res = (*fres).get() ;
				std::cout << "#############################################################\n\
				Testing of " << res.componentId << " completed: " << res.globalResult << std::endl ;
					
				if ( verbose )
					{
					for ( auto tr: res.componentResult.test_results )
						{
						std::cout  << "\tStdOut:\n\t" << tr.stdout << "\n" ;
						std::cout  << "\tStdErr:\n\t" << tr.stderr << "\n" ;
						}
					}
				} ;

			std::cerr << "\n#############################################################\n" ;
			continue;
		}
		if ( in == "tree" ){
			if ( tree )
				std::cout << tree << std::endl;
			else
				std::cout << "(empty tree)" << std::endl;
			continue;
		}
		if ( in == "help" )
		{
			printUsage();
			continue ;
		}
		if ( in == "verbose" )
		{
			dvsm->discardTestOutput(false) ;
			verbose = 1 ;
			continue ;
		}
		if ( in == "silent" )
		{
			dvsm->discardTestOutput(true) ;
			verbose = 0 ;
			continue;
		}
		if ( in == "" )
			continue;
		
		std::cerr << "(invalid command " << in << ")" << std::endl;
	} ;

	delete dvsm ;
	return 0;
}
