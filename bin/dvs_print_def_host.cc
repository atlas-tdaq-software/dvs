#include <cmdl/cmdargs.h>
#include <system/Host.h>
#include <config/Configuration.h>
#include <dal/util.h>
#include <dal/Partition.h>
#include <dal/Computer.h>

int main ( int argc, char **  argv)
{ 
	CmdArgStr	data_file('d', "data", "data-file-name", "Configuration to test.", CmdArg::isREQ);
	CmdArgStr	partition('p', "partition", "partition", "selected partition ", CmdArg::isREQ);

      // Declare command object and its argument-iterator
	CmdLine  cmd(*argv, &data_file, &partition, NULL);
	cmd.description("Prints out host name and RLogin attribute of the DefaultHost of the given partition. "
		        "This host is used as default host by TestManager (and DVS) to start tests.") ;
	CmdArgvIter  arg_iter(--argc, ++argv);
  
       // Parse arguments
	cmd.parse(arg_iter);

	Configuration* confdb = new Configuration(std::string("oksconfig:")+std::string(data_file));	

	if ( !confdb->loaded() )
		{
		std::cerr	<< "Failed to load configuration from file "
				<< data_file << std::endl ;
		return 1 ;
		} ;

//	std::string pname = partition ;
	const daq::core::Partition* partition_ = daq::core::get_partition(*confdb, (const char*)partition) ;

	if ( !partition_ )
		{
                std::cerr 	<< "ERROR [DVSManager::loadDatabase] Partition " << partition
				<< " not found in the database\n" ;
                return 1 ;
		}

	std::string myHostName;
	if ( partition_->get_DefaultHost() )
		std::cout << partition_->get_DefaultHost()->get_RLogin() << " " << partition_->get_DefaultHost()->UID() << std::endl ;
	else
	  {
	    ERS_DEBUG(1,"\nDefault host not defined in the database. Use local host.");
	    
	    myHostName = System::LocalHost::full_local_name();
	    
	    try {
	      const daq::core::Computer * def_host = confdb->get<daq::core::Computer>(myHostName) ;
	      std::cout << def_host->get_RLogin() << " " << def_host->UID() << std::endl ;
	    }
	    catch (ers::Issue &e) {
	      std::cerr << "\nLocal host "<< myHostName <<" is not defined in the database."<< std::endl;
	      return -1;
	    }
	  }
	return 0 ;
}
