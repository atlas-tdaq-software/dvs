#!/bin/sh

database=../data/onlsw_test.data.xml
partition="onlsw_test"

echo "TestDAL check starting"

./testdal_try -d $database -p $partition || { echo "ERROR:: TestDAL check FAILED"; exit 1; }

echo "TestDAL check PASSED"
