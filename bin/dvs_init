#!/bin/sh

function print_usage
{
	echo "dvs_init: utility to start default IPC server and PMG Agents on all hosts used by the partition, \
that are needed to run DVS."
	echo 
	echo "Usage: "
	echo "dvs_init [database_file] partition"
	echo "database_file 		configuration database"
	echo "               		(you must specify it unless you have TDAQ_DB_DATA defined)"
	echo "               		(can be specified relatively to cwd or to TDAQ_DB_PATH)"
	echo "partition 		selected partition (required parameter)"
}

#------------------------------------------------------------
# parse command line
#------------------------------------------------------------

while (test $# -gt 0 ); do
  case "$1" in
    -h | --help)
	print_usage
	exit 0
    ;;

    *.xml)
	TDAQ_DB_DATA=$1
	shift
    ;;

    *)
	partition=$1
	shift
    ;;
  esac
done

if test -z "${TDAQ_DB_DATA}"
  then
	echo "Error: Neither database file parameter nor TDAQ_DB_DATA environment are defined."
	print_usage
	exit 1
fi

if test -z "$partition"
  then
	echo "Error: partition parameter not specified. Please specify a partition you want to run."
	print_usage
	exit 1
fi

echo "dvs_init is launching pmg_agent"
TDAQ_PARTITION=$partition
export TDAQ_PARTITION TDAQ_DB_DATA

#------------------------------------------------------------
# get TDAQ environment
#------------------------------------------------------------

if test -r "${TDAQ_INST_PATH}/share/bin/setup_functions"
then
	source ${TDAQ_INST_PATH}/share/bin/setup_functions; 
	get_partition_env $TDAQ_PARTITION;
else
	echo "Can not access $TDAQ_INST_PATH/share/bin/setup_functions. Did you set up TDAQ-SW release?"
	exit 1
fi

check_with()
{
	count=0
	until $@
	do
		sleep 1
		count=`expr $count + 1`

		if test $count -eq 15
		then
   			echo "Timeout!"
			echo "(Server was not started)"
			exit 1
		fi
	done
}

echo -n "Getting partition default host:  "
export TDAQ_ERS_DEBUG_LEVEL=0
defhostname=`dvs_print_def_host -d ${TDAQ_DB_DATA} -p $partition` || { echo "Failed to get local host. Check your database" ; exit 1 ; }
hostname=`echo $defhostname | sed 's/^.* //'`
if test "$defhostname" = ""; then hostname=`hostname -f`; fi
echo $hostname
echo "Getting all hosts used in the partition:"
dvs_get_hosts -d ${TDAQ_DB_DATA} -p $partition > /tmp/${USER}_hosts || \
{ echo "Failed to get list of hosts in the partition. Check your database" ; exit 1 ; }

cat /tmp/${USER}_hosts

echo "Initializing DVS environment:"

if ! test_ipc_server; then
ipc_server >/dev/null 2>&1 &
echo -n " waiting for General partition server to startup ..."
check_with test_ipc_server
fi

#     DEF_PMGLOGSPATH="/tmp"
#     PMG_LOGS_PATH=${TDAQ_LOGS_ROOT-${DEF_PMGLOGSPATH}}
#     umask 002; mkdir -p ${PMG_LOGS_PATH} || { echo "can not create dir ${PMG_LOGS_PATH}"; exit ; }
PMG_LOGS_PATH="/tmp"

SSH_OPTIONS="-o StrictHostKeyChecking=no -o ConnectTimeout=5"
FILE=/tmp/${USER}_hosts
while read LINE ; do
#echo $LINE
    pmg_log="${PMG_LOGS_PATH}/pmgserver_${USER}_$LINE.out"
    pmg_binary=`which pmgserver`
    tdaq_inst_path="${TDAQ_INST_PATH}"
    tdaq_ipc_ref="${TDAQ_IPC_INIT_REF}"

    if ! test_pmg_agent -H $LINE -t 10; then
      echo " waiting for pmgserver to startup on $LINE (log file $pmg_log) ..."

      if test $LINE = `hostname -f`; then
         $pmg_binary $PMG_AGENT_OPTS > $pmg_log 2>&1 &    
      else	 
	 ssh ${SSH_OPTIONS} $LINE -nf "/bin/sh -c 'mkdir -p ${PMG_LOGS_PATH}; source ${tdaq_inst_path}/setup.sh; \
	 export TDAQ_IPC_INIT_REF="${tdaq_ipc_ref}"; \
         pmgserver $PMG_AGENT_OPTS </dev/null >$pmg_log 2>&1 &' "	 
      fi
    else
      echo " pmgserver already running on host" $LINE
    fi 
done < $FILE
/bin/rm -f /tmp/${USER}_hosts 

sleep 3

echo "done."
echo "Now you can execute dvs_gui to start DVS"
