#include <unistd.h>
#include <cmdl/cmdargs.h>
#include <ers/ers.h>
#include <system/Host.h>
#include <config/Configuration.h>
#include <dal/util.h>
#include <dal/Partition.h>
#include <dal/Computer.h>
#include <dal/Application.h>
//#include <dal/TemplateApplication.h>
//#include <dal/HLTSegment.h>


int main ( int argc, char **  argv)
{ 
  CmdArgStr	data_file('d', "data", "data-file-name", "Configuration database file", CmdArg::isREQ);
  CmdArgStr	partition('p', "partition", "partition", "Selected partition", CmdArg::isREQ);

  CmdLine  cmd(*argv, &data_file, &partition, NULL);
  cmd.description("Print out all hosts names (used in the partition) and their RLogin attributes") ;
  CmdArgvIter  arg_iter(--argc, ++argv);
  cmd.parse(arg_iter);

  Configuration* confdb = new Configuration(std::string("oksconfig:")+std::string(data_file));	

  if ( !confdb->loaded() )
  {
       ERS_DEBUG(0, "ERROR: Failed to load configuration from file "<< (const char*)data_file );
       return -1 ;
  }
  const daq::core::Partition* partition_ = daq::core::get_partition(*confdb, (const char*)partition) ;
  if ( !partition_ )
  {
       ERS_DEBUG(0, "ERROR: Partition "<<partition<<" not found in the database." );
       return -1 ;
  }

  std::string myHostName;
  const daq::core::Computer * def_host =0;  
  if ( !(partition_->get_DefaultHost()) )
  {
      ERS_DEBUG(1,"\nDefault host not defined in the database. Use local host.");
      
      myHostName = System::LocalHost::full_local_name();
      
      try {
	def_host = confdb->get<daq::core::Computer>(myHostName) ;
      }
      catch (ers::Issue &e) {
	ERS_DEBUG(1,"\nLocal host "<<myHostName<<" is not defined in the database.");
	return -1;
      }
  }
  else
  {
      ERS_DEBUG(1,"\nDefault host name "<< partition_->get_DefaultHost()->UID() ) ; 
      def_host = partition_->get_DefaultHost();
  }

   std::set<std::string> hosts_names;
   std::vector<const daq::core::BaseApplication*> apps;
   try	{
	apps = partition_->get_all_applications() ;
	}
   catch ( ers::Issue & ex )
	{
	ers::error ( ex ) ;
	return -1 ;
	}

   for ( const auto app: apps )
	{
	if( app->get_host()->get_State() == true )
	       hosts_names.insert(app->get_host()->UID());
	}	  
 
   if (def_host->get_State()==true )
      hosts_names.insert(def_host->UID());   
   std::set<std::string>::const_iterator pos=hosts_names.begin();
   for(; pos!=hosts_names.end();++pos)
   {
     std::cout<<*pos<<std::endl;
   }

     
  return 0 ;
}
