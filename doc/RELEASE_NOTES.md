# DVS (Diagnostics and Verification Framework)

## tdaq-11-02-00

Do not expose TM/Client.h from manager.h (better separation of implementation from interface).

## tdaq-09-00-00

### Replacement of CLIPS

Full re-implementation of the expert-system engine: CLIPS (an old forward-chainging C rule engine) was replaced by a custom forward-chaining engine where rules can be defined as C++ objects (relying on C++11 features like lambda). The rules are compiled and linked to the executable. The present set of rules is to handle Test policies and dependencies:  [rules](../src/KB.cpp).

### TestFailure follow-up actions

A change in TestRepository schema related to description of `TestFailureActions` is described in TM release notes and in main DVS/TM [twiki](https://twiki.cern.ch/twiki/bin/view/Atlas/DaqHltTestManager#How_to_Configure_Tests "Configuring tests"). An Action is prepared by DVS from this configuration and added to the test result passed to client (RC, CHIP).

### Internal notes

#### Use of new DAL API

Avoid using Config layer (in favor of Dal) and AppConfig/SegConfig classes (in favor of plain Segment and Application), making loading of testable tree more uniform.


#### limitations and future developments

Make possible to load rules from user-supplied .so files at runtime, just passing file names as parameter to DVS engine. 
